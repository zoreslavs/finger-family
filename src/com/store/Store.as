﻿package com.store
{
	import com.store.handlers.AndroidIAP;
	import com.store.handlers.AppleIAP;
	import com.store.handlers.DummyIAP;
	import com.utils.PlatformCheckHelper;
	
	import flash.events.EventDispatcher;
	import flash.utils.setTimeout;
	
	public class Store extends EventDispatcher
	{
		private static var _instance:Store;
		
		private var _canMakePurchase:Boolean;
		private var _iapHandler:IAPHandler;
		private var _successCallback:Function;
		private var _failCallback:Function;
		private var _cancelCallback:Function;
		private var _isInitialized:Boolean;
		
		public function Store(isNetworkConnected:Boolean)
		{
			if (isNetworkConnected)
				init();
		}
		
		/**
		 * @param String platform : name of platform (ios, android)
		 * Initialize store class, which creates and bind proper
		 * IAP helper then.
		 */
		public function init():void
		{
			if (_isInitialized)
				return;
			
			_isInitialized = true;
			
			//Based on platform, create IAPHandler
			if (PlatformCheckHelper.isIOS)
				_iapHandler = new AppleIAP();
			else if (PlatformCheckHelper.isAndroid)
				_iapHandler = new AndroidIAP();
			else 
				_iapHandler = new DummyIAP();
			
			if (_iapHandler)
			{
				//Send init signal to IAP handler.
				_iapHandler.addEventListener(StoreEvent.IAP_INIT_SUCCESS, onIAPInitSuccess);
				_iapHandler.addEventListener(StoreEvent.IAP_INIT_FAIL, onIAPInitFail);
				_iapHandler.addEventListener(StoreEvent.IAP_RESTORE_SUCCESS, onRestoreSuccess);
				_iapHandler.addEventListener(StoreEvent.IAP_RESTORE_FAIL, onRestoreFail);
				_iapHandler.init();
			}
		}
		
		/**
		 * @desc
		 * @param
		 * @return
		 */
		public function get isSupported():Boolean
		{
			return false;
		}
		
		/**
		 * @desc String item_id to purchase
		 * @param
		 * @return
		 */
		public function makePurchase(successCallback:Function, failCallback:Function, cancelCallback:Function):void
		{
			_successCallback = successCallback;
			_failCallback = failCallback;
			_cancelCallback = cancelCallback;
			
			if (PlatformCheckHelper.isIOS)
			{
				if (!_canMakePurchase)
				{
					log("Init store api first.");
					return;
				}
			}
			_iapHandler.addEventListener(StoreEvent.IAP_PURCHASE_SUCCESS, onPurchaseSuccess);
			_iapHandler.addEventListener(StoreEvent.IAP_PURCHASE_FAIL, onPurchaseFail);
			_iapHandler.addEventListener(StoreEvent.IAP_PURCHASE_CANCELLED, onPurchaseCancel);
			_iapHandler.purchase(IAPData.getProductID());
		}
		
		private function onPurchaseCancel(e:StoreEvent):void
		{
			//PurchaseInstance.hideAnim();
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_CANCELLED, onPurchaseCancel);
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_SUCCESS, onPurchaseSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_FAIL, onPurchaseFail);
			
			_cancelCallback.call();
		}
		
		/**
		 * @desc
		 * @param
		 * @return
		 */
		public function restorePurchases():void
		{
			log("Restore purchases: " + _canMakePurchase);
			
			if (PlatformCheckHelper.isIOS)
			{
				//PurchaseInstance.showAnim();
			}
			if (PlatformCheckHelper.isAndroid)
			{
				if (!_canMakePurchase)
				{
					log("Init store api first");
					return;
				}
			}
			_iapHandler.addEventListener(StoreEvent.IAP_RESTORE_SUCCESS, onRestoreSuccess);
			_iapHandler.addEventListener(StoreEvent.IAP_RESTORE_FAIL, onRestoreFail);
			_iapHandler.restore();
		}
		
		//////////////////////////Private methods//////////////////////
		
		/**
		 * @desc e StoreEvent
		 * @param
		 * @return
		 */
		private function onIAPInitSuccess(e:StoreEvent):void
		{
			log("Init Success");
			_iapHandler.removeEventListener(StoreEvent.IAP_INIT_SUCCESS, onIAPInitSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_INIT_FAIL, onIAPInitFail);
			
			_canMakePurchase = true;
			if (PlatformCheckHelper.isAndroid || PlatformCheckHelper.isAmazon) {
				setTimeout(restorePurchases, 2000);
			}
		}
		
		/**
		 * @desc e StoreEvent
		 * @param
		 * @return
		 */
		private function onIAPInitFail(e:StoreEvent):void
		{
			log("Init Fail");
			_iapHandler.removeEventListener(StoreEvent.IAP_INIT_SUCCESS, onIAPInitSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_INIT_FAIL, onIAPInitFail);
			_canMakePurchase = false;
		}
		
		/**
		 * @desc e StoreEvent
		 * @param
		 * @return
		 */
		private function onPurchaseSuccess(e:StoreEvent):void
		{
			log("Purchase succeed: " + e.data);
			//PurchaseInstance.hideAnim();
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_CANCELLED, onPurchaseCancel);
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_SUCCESS, onPurchaseSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_FAIL, onPurchaseFail);
			
			_successCallback.call();
		}
		
		/**
		 * @desc e StoreEvent
		 * @param
		 * @return
		 */
		private function onPurchaseFail(e:StoreEvent):void
		{
			log("Purchase failed.");
			//PurchaseInstance.hideAnim();
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_CANCELLED, onPurchaseCancel);
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_SUCCESS, onPurchaseSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_PURCHASE_FAIL, onPurchaseFail);
			
			_failCallback.call();
		}
		
		/**
		 * @desc e StoreEvent
		 * @param
		 * @return
		 */
		private function onRestoreSuccess(e:StoreEvent):void
		{			
			log("Restore succeed.");
			//PurchaseInstance.hideAnim();
			_iapHandler.removeEventListener(StoreEvent.IAP_RESTORE_SUCCESS, onRestoreSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_RESTORE_FAIL, onRestoreFail);
		}
		
		/**
		 * @desc e StoreEvent
		 * @param
		 * @return
		 */
		private function onRestoreFail(e:StoreEvent):void
		{
			log("Restore Fail");
			//PurchaseInstance.hideAnim();
			_iapHandler.removeEventListener(StoreEvent.IAP_RESTORE_SUCCESS, onRestoreSuccess);
			_iapHandler.removeEventListener(StoreEvent.IAP_RESTORE_FAIL, onRestoreFail);
		}
		
		/** Log */
		private function log(msg:String):void
		{
			trace("[Store] " + msg);
		}
	}
}