﻿package com.store.handlers
{
	import com.milkmangames.nativeextensions.android.AndroidIAB;
	import com.milkmangames.nativeextensions.android.AndroidItemDetails;
	import com.milkmangames.nativeextensions.android.AndroidPurchase;
	import com.milkmangames.nativeextensions.android.events.AndroidBillingErrorEvent;
	import com.milkmangames.nativeextensions.android.events.AndroidBillingEvent;
	import com.store.IAPData;
	import com.store.IAPHandler;
	import com.store.StoreEvent;
	
	import flash.events.EventDispatcher;

	public class AndroidIAP extends EventDispatcher implements IAPHandler 
	{
		private static const PUBLIC_KEY:String = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuEPsbkOd6PZXj5FrmfmHrdcSkZAG9JrtCE5YMbbFdFzQjN/3tqvNywAx0pAzHZfdcX6jIEqnNRF+tljuHmrVOgHP250bOcmIbAQq6nupx+TMuW5W1aj3D5JT9hSL/ERrVuoGLo1GQx2fS62ntlSJRqYpg2NKFCwcPs9a2B0vwdpL60PCmpZEeT0TvBWpkerYqOrqzcV0Zf6o3n4uC5AcC/hHukJTFuyDayO9RZ09qoS1Ys4pATAd/epAEZ48b+bdnYd4+l0fgwxY8uW0mqvPDy7m5fNmZmtJKlwV1pxTao/DuMfJXWweonopmlExCL+Omr0Kt8/soz19S5/dgjqm7wIDAQAB";
		
		private var instance:AndroidIAP;
		
		private var loadingVisible:Boolean;
		private var canMakePurchase:Boolean;
		private var canRestoreTransaction:Boolean;
		
		private static var obj:Object;
		private var productId:String;
		private var key:Number;
		
		/** My Purchases */
		private var myPurchases:Vector.<AndroidPurchase>;
		
		private var productIdList:Vector.<String> = new Vector.<String>();
		
		public function AndroidIAP()
		{
			instance = this;
		}
		
		public function init():void
		{
			canRestoreTransaction = false;
			canMakePurchase = false;
			
			if (!AndroidIAB.isSupported())
			{
				log("Android Billing is not supported on this platform.");
				return;
			}
				
			log("Initializing Android billing..");		
			AndroidIAB.create();
			log("AndroidIAB Initialized.");
			
			// listeners for billing service startup
			AndroidIAB.androidIAB.addEventListener(AndroidBillingEvent.SERVICE_READY,onServiceReady);
			AndroidIAB.androidIAB.addEventListener(AndroidBillingEvent.SERVICE_NOT_SUPPORTED, onServiceUnsupported);
			
			// listeners for making a purchase
			AndroidIAB.androidIAB.addEventListener(AndroidBillingEvent.PURCHASE_SUCCEEDED,onPurchaseSuccess);
			AndroidIAB.androidIAB.addEventListener(AndroidBillingErrorEvent.PURCHASE_FAILED, onPurchaseFailed);
		
			// listeners for player's owned items
			AndroidIAB.androidIAB.addEventListener(AndroidBillingEvent.INVENTORY_LOADED, onInventoryLoaded);
			AndroidIAB.androidIAB.addEventListener(AndroidBillingErrorEvent.LOAD_INVENTORY_FAILED, onInventoryFailed);
			
			// listeners for item details
			AndroidIAB.androidIAB.addEventListener(AndroidBillingEvent.ITEM_DETAILS_LOADED, onItemDetails);
			AndroidIAB.androidIAB.addEventListener(AndroidBillingErrorEvent.ITEM_DETAILS_FAILED, onDetailsFailed);
			
		    // listeners for consuming an item
			AndroidIAB.androidIAB.addEventListener(AndroidBillingEvent.CONSUME_SUCCEEDED, onConsumed);
			AndroidIAB.androidIAB.addEventListener(AndroidBillingErrorEvent.CONSUME_FAILED, onConsumeFailed);
		
			// start the  billing service.  in onServiceReady(), we'll update everything else.
			log("Starting billing service...");
			AndroidIAB.androidIAB.startBillingService(PUBLIC_KEY);
			log("Waiting for biling response...");
		}
		
		public function purchase(item_id:String):void
		{
			// for this to work, you must have added 'my_levelpack' in the google play website
			if (canMakePurchase)
			{
				log("Purchase: " + item_id);
				AndroidIAB.androidIAB.purchaseItem(item_id);
				log("Waiting for purchase response...");
			}
		}
		
		public function restore():void
		{
			log("Restore function of AndroidIAP called.");
			AndroidIAB.androidIAB.loadPlayerInventory();
		}
		
		/** Dispatched when the billing service is started -now you can make other calls */
		private function onServiceReady(e:AndroidBillingEvent):void
		{
			// as soon as it starts, we load up the player's inventory to get a list of what they own
			log("Service ready. Loading inventory...");
			canMakePurchase = true;
			productIdList.push(IAPData.getProductID());
			AndroidIAB.androidIAB.loadItemDetails(productIdList);
			AndroidIAB.androidIAB.loadPlayerInventory();//added by me not in orig			
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_INIT_SUCCESS));
		}
        
		/** This will be triggered if Google Play billing is not supported on the device. */
		private function onServiceUnsupported(e:AndroidBillingEvent):void
		{
			log("Billing service not supported.");
			canMakePurchase = false;
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_INIT_FAIL));
		}
		
		/** Callback when the player's inventory has been loaded, after calling loadPlayerInventory() */	
		private function onInventoryLoaded(e:AndroidBillingEvent):void
		{
			log("Inventory updated: " + e.purchases);
			this.myPurchases = e.purchases;
			updateInventoryMessage();
		}
		
		/** Update Inventory Message */
		private function updateInventoryMessage():void
		{
			var allOwnedItemIds:Vector.<String> = new Vector.<String>();
			log("Inside updateInventoryMessage: " + allOwnedItemIds)
			if (myPurchases==null)
			{
				log("Inventory: (not loaded)")
				return;
			}
			if (!canRestoreTransaction)
			{
				log("Restore......");
				restoreData(allOwnedItemIds);
			}
			else
			{
				log("Normal game flow......");
				updateData();	
			}			
			log("Inventory: {"+allOwnedItemIds.join(", ")+"}");
		}
		
		//restore data
		private function restoreData(allOwnedItemIds:Vector.<String>):void
		{
			canRestoreTransaction = true;
			for each(var purchase:AndroidPurchase in myPurchases)
			{
				this.dispatchEvent(new StoreEvent(StoreEvent.IAP_RESTORE_SUCCESS, purchase.itemId));
				log("Inside restore: " + purchase.itemType + ", " + purchase.itemId);
				AndroidIAB.androidIAB.consumeItem(purchase.itemId);
			}
		}
		
		private function updateData():void
		{
			for each(var purchase:AndroidPurchase in myPurchases)
			{
				if (purchase.itemId == productId)
				{	
					if (myPurchases)
					{
						log("You already own this item!");
					}
					
					for each(var id:String in productIdList)
					{
						log("ID: " + id);
						if (purchase.itemId == id)
						{
							log("Consuming items: " + purchase.itemId);
							AndroidIAB.androidIAB.consumeItem(purchase.itemId);
						}
					}
					this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_SUCCESS, purchase.itemId));
					productId = "";
					return;
				}
			}
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_FAIL));
		}
		
		/** Getting the inventory failed */
		private function onInventoryFailed(e:AndroidBillingErrorEvent):void
		{
			log("Failed loading inventory: " + e.errorID + "/" + e.text);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_FAIL));
		}
		
		/** Details about the available items is loaded */
		private function onItemDetails(e:AndroidBillingEvent):void
		{
			log("Details: [" + e.itemDetails.length + "]=" + e.itemDetails.join(","));
			var arr:Array = [];
			// the itemDetails property now contains the item info
			for each(var item:AndroidItemDetails in e.itemDetails)
			{
				trace("ID: " + item.itemId);
				trace("Title: " + item.title);
				trace("Description: " + item.description);
				trace("Price: " + item.price);
				trace("Price Currency Code: " + item.priceCurrencyCode);
			}	
		}
		
		/** An error occurred loading the item details */
		private function onDetailsFailed(e:AndroidBillingErrorEvent):void
		{
			log("Error loading details: " + e.errorID + "/" + e.text);
		}
	    
		/** Called after a successful item purchase. */
		private function onPurchaseSuccess(e:AndroidBillingEvent):void
		{
			log("Successful purchase of '" + e.itemId + "' = " + e.purchases[0]);
			// every time a purchase is updated, refresh your inventory from the server.
			reloadInventory();
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_SUCCESS, e.itemId));
		}
		
		/** This is called when an error occurs trying to buy something */
		private function onPurchaseFailed(e:AndroidBillingErrorEvent):void
		{
			log("Failure purchasing '" + e.itemId + "', reason: " + e.errorID);
			if (e.errorID == 7)
			{				
				log("Call for restore purchases: " +  e.itemId);
				this.dispatchEvent(new StoreEvent(StoreEvent.IAP_RESTORE_SUCCESS, e.itemId));
			}
			else
			{
				this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_FAIL));
			}
		}
		
		/** Refresh the player's inventory from the server.  */
		private function reloadInventory():void
		{
			log("Loading inventory...");
			AndroidIAB.androidIAB.loadPlayerInventory();
			log("Waiting for inventory...");
		}
		
		/** An Item was successfully consumed */
		private function onConsumed(e:AndroidBillingEvent):void
		{
			log("Did consume item: " + e.itemId);
			// reload inventory now that it has changed
			AndroidIAB.androidIAB.loadPlayerInventory();
		}

		/** Attempt to consume item has failed */
		private function onConsumeFailed(e:AndroidBillingErrorEvent):void
		{
			log("Consume spell failed: " + e.errorID + "/" + e.text);
		}
		
		/** Log */
		private function log(msg:String):void
		{
			trace("[Store GoogleIAP] " + msg);
		}	
	}
}