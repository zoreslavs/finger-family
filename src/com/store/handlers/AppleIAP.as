﻿package com.store.handlers
{
	import com.milkmangames.nativeextensions.ios.StoreKit;
	import com.milkmangames.nativeextensions.ios.StoreKitProduct;
	import com.milkmangames.nativeextensions.ios.events.StoreKitErrorEvent;
	import com.milkmangames.nativeextensions.ios.events.StoreKitEvent;
	import com.store.IAPData;
	import com.store.IAPHandler;
	import com.store.StoreEvent;
	
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	public class AppleIAP extends EventDispatcher implements IAPHandler
	{
		private var completePurchases:Dictionary = new Dictionary();
		private var failedPurchases:Dictionary = new Dictionary();
		private var restoredPurchases:Dictionary = new Dictionary();
		private var isInAppPurchaseInitialized:Boolean = false;
		
		public function init():void
		{
			log("Init");
			StoreKit.create();
			var isSupported:Boolean = StoreKit.isSupported();
			
			log("Version: " + StoreKit.VERSION + ", supported: " + isSupported);
			
			if (!StoreKit.storeKit.isStoreKitAvailable())
			{
				log("Store is disable on this device.");
				return;
			}
			
			if (isSupported && !isInAppPurchaseInitialized)
			{
				log("StoreKit Initialized.");
				
				isInAppPurchaseInitialized = true;
				// add listeners here
				StoreKit.storeKit.addEventListener(StoreKitEvent.PRODUCT_DETAILS_LOADED, onProductsLoaded);
				StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_SUCCEEDED, onPurchaseSuccess);
				StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_CANCELLED, onPurchaseUserCancelled);
				StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_DEFERRED, onPurchaseDeferred);
				StoreKit.storeKit.addEventListener(StoreKitEvent.TRANSACTIONS_RESTORED, onTransactionsRestored);
				
				// adding error events. always listen for these to avoid your program failing.
				StoreKit.storeKit.addEventListener(StoreKitErrorEvent.PRODUCT_DETAILS_FAILED, onProductDetailsFailed);
				StoreKit.storeKit.addEventListener(StoreKitErrorEvent.PURCHASE_FAILED, onPurchaseFailed);
				StoreKit.storeKit.addEventListener(StoreKitErrorEvent.TRANSACTION_RESTORE_FAILED, onTransactionRestoreFailed);
				var productIdList:Vector.<String> = new Vector.<String>();
				productIdList.push(IAPData.getProductID());
				StoreKit.storeKit.setManualTransactionMode(true);
				StoreKit.storeKit.loadProductDetails(productIdList);
				this.dispatchEvent(new StoreEvent(StoreEvent.IAP_INIT_SUCCESS));
			}
		}
		
		public function purchase(item_id:String):void
		{
			log("Purchase: " + item_id);
			StoreKit.storeKit.purchaseProduct(item_id);
		}
		
		public function restore():void
		{
			log("Restore");
			StoreKit.storeKit.restoreTransactions();
		}
		
		/** Called when details about available purchases has loaded */
		private function onProductsLoaded(e:StoreKitEvent):void
		{
			log("Products loaded.");
			for each (var product:StoreKitProduct in e.validProducts)
			{
				log("LocaleID: " + product.localeId);
				log("ID: " + product.productId);
				log("Title: " + product.title);
				log("Description: " + product.description);
				log("String Price: " + product.localizedPrice);
				log("Price: " + product.price)
			}
			log("Loaded " + e.validProducts.length + " products.");
			
			// if any of the product ids we tried to pass in were not found on the server,
			// we won't be able to by them so something is wrong.
			if (e.invalidProductIds.length > 0)
			{
				log("Error! These products not valid: " + e.invalidProductIds.join(","));
				return;
			}
			
			log("Ready! Hosted content supported: " + StoreKit.storeKit.isHostedContentAvailable());
		}
		
		/** A purchase has succeed */
		private function onPurchaseSuccess(e:StoreKitEvent):void
		{
			// show purchase success popup
			log("Purchase success: " + e.productId);
			StoreKit.storeKit.manualFinishTransaction(e.transactionId);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_SUCCESS, e.productId));
		}
		
		/** Called when product details failed to load */
		private function onProductDetailsFailed(e:StoreKitErrorEvent):void
		{
			log("Error while loading products: " + e.text);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_FAIL, e));
		}
		
		/** A purchase has failed */
		private function onPurchaseFailed(e:StoreKitErrorEvent):void
		{
			log("Purchase failed: " + e.productId + ", transactionId: " + e.transactionId + ", originalTransactionId: " + e.originalTransactionId);
			StoreKit.storeKit.manualFinishTransaction(e.transactionId);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_FAIL, e.productId));
		}
		
		/** A purchase was cancelled */
		private function onPurchaseUserCancelled(e:StoreKitEvent):void
		{
			log("Purchase cancelled: " + e.productId + ", " + e);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_PURCHASE_CANCELLED));
		}
		
		/** A Purchase Was deferred */
		private function onPurchaseDeferred(e:StoreKitEvent):void
		{
			log("Purchase deferred: " + e.productId + ", " + e);
		}
		
		/** All transactions have been restored */
		private function onTransactionsRestored(e:StoreKitEvent):void
		{
			log("All previous transactions restored! " + e.productId + ", " + e.transactionId);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_RESTORE_SUCCESS, e.productId));
		}
		
		/** Transaction restore has failed */
		private function onTransactionRestoreFailed(e:StoreKitErrorEvent):void
		{
			log("Error while restoring purchases: " + e.text);
			this.dispatchEvent(new StoreEvent(StoreEvent.IAP_RESTORE_FAIL, e));
		}
		
		/** Log */
		private function log(msg:String):void
		{
			trace("[Store AppleIAP] " + msg);
		}
	}
}