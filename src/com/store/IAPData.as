package com.store
{
	import com.Application;
	import com.utils.PlatformCheckHelper;

	public class IAPData
	{
		public static const APP_STORE_PRODUCT_ID:String = "com.fingerfamilyapp.fingerfamily.sharevideo";
		public static const GOOGLE_PLAY_PRODUCT_ID:String = "com.fingerfamilyapp.fingerfamily.sharevideo";
		
		public static function getProductID():String
		{
			var result:String;
			if (PlatformCheckHelper.isIOS)
				result = APP_STORE_PRODUCT_ID;
			else if (PlatformCheckHelper.isAndroid)
				result = GOOGLE_PLAY_PRODUCT_ID;
			else 
				result = APP_STORE_PRODUCT_ID;
			
			return result;
		}
	}
}