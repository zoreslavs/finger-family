package com.abstract
{
	public class FamilySession
	{
		public var time:Number;
		public var images:Array;
		public var voices:Array;
		public var types:Array;
		public var hand:int;
		public var background:int;
		public var videoTime:Number;
		
		public function FamilySession(time:Number, images:Array, voices:Array, types:Array, hand:int, background:int)
		{
			this.time = time;
			this.images = images;
			this.voices = voices;
			this.types = types;
			this.hand = hand;
			this.background = background;
		}
	}
}