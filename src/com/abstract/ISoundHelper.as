package com.abstract
{
	import com.rainbowcreatures.FWVideoEncoder;

	public interface ISoundHelper
	{
		function init(fwEncoder:FWVideoEncoder, onHandAnimCallback:Function, onZoomAnimCallback:Function, onRepeatAnimCallback:Function, onAudioFinishCallback:Function):void;
		function start():void;
		function stop():void;
		function initSoundQueue(completeCallback:Function):void;
	}
}