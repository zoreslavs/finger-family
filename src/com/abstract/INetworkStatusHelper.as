package com.abstract
{
	public interface INetworkStatusHelper
	{
		function get isConnected():Boolean;
	}
}