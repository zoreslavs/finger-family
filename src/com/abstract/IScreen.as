package com.abstract
{
	import flash.display.MovieClip;

	public interface IScreen
	{
		function get content():MovieClip;
		function init():void;
		function dispose():void;
	}
}