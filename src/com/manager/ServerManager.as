package com.manager
{
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class ServerManager
	{
		private var _phpFileRequest:URLRequest;
		private var _phpLoader:URLLoader;
		
		private var _isLogin:Boolean;
		private var _onSuccessCallback:Function;
		private var _onFailCallback:Function;
		private var _onErrorCallback:Function;
		
		public function ServerManager()
		{
			_phpLoader = new URLLoader();
			_phpLoader.dataFormat = URLLoaderDataFormat.TEXT;
			_phpLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			_phpLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			_phpLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, onHTTPResponseStatus);
			_phpLoader.addEventListener(Event.COMPLETE, onResultGet);
		}
		
		public function loginUser(userName:String, password:String, onSuccess:Function, onFail:Function, onError:Function):void
		{
			trace("[Server] Login: ", userName, password);
			_onSuccessCallback = onSuccess;
			_onFailCallback = onFail;
			_onErrorCallback = onError;
			
			_isLogin = true;
			
			var phpVars:URLVariables = new URLVariables();
			_phpFileRequest = new URLRequest("http://fingerfamilyapp.com/controlpanel.php");
			_phpFileRequest.method = URLRequestMethod.POST;
			_phpFileRequest.data = phpVars;
			
			phpVars.systemCall = "checkLogin";
			phpVars.username = userName;
			phpVars.password = password;
			
			_phpLoader.load(_phpFileRequest);
		}
		
		public function checkUser(userName:String, onSuccess:Function, onFail:Function, onError:Function):void
		{
			trace("[Server] Check user: ", userName);
			_onSuccessCallback = onSuccess;
			_onFailCallback = onFail;
			_onErrorCallback = onError;
			
			_isLogin = true;
			
			var phpVars:URLVariables = new URLVariables();
			_phpFileRequest = new URLRequest("http://fingerfamilyapp.com/checkuser.php");
			_phpFileRequest.method = URLRequestMethod.POST;
			_phpFileRequest.data = phpVars;
			
			phpVars.systemCall = "checkUser";
			phpVars.username = userName;
			
			_phpLoader.load(_phpFileRequest);
		}
		
		public function createUser(userName:String, password:String, onSuccess:Function, onFail:Function, onError:Function):void
		{
			trace("[Server] Create user: ", userName, password);
			
			_onSuccessCallback = onSuccess;
			_onFailCallback = onFail;
			_onErrorCallback = onError;
			
			_isLogin = false;
			
			var phpVars:URLVariables = new URLVariables();
			_phpFileRequest = new URLRequest("http://fingerfamilyapp.com/adduser.php");
			_phpFileRequest.method = URLRequestMethod.POST;
			_phpFileRequest.data = phpVars;
			
			phpVars.systemCall = "createUser";
			phpVars.username = userName;
			phpVars.password = password;
			
			_phpLoader.load(_phpFileRequest);
		}
		
		protected function onIOError(e:IOErrorEvent):void
		{
			trace("[ServerManager] onIOError: ", e.text);
			_onErrorCallback.call();
		}
		
		protected function onSecurityError(e:SecurityErrorEvent):void
		{
			trace("[ServerManager] onSecurityError: ", e.text);
			_onErrorCallback.call();
		}
		
		protected function onHTTPResponseStatus(e:HTTPStatusEvent):void
		{
			trace("[ServerManager] onHTTPResponseStatus: ", e.status);
		}
		
		protected function onResultGet(e:Event):void
		{
			var resultStr:String = JSON.stringify(e.target.data);
			var faultStr:String;
			var isSuccess:Boolean;
			if (_isLogin)
			{
				faultStr = "User doesn't exist.";
				isSuccess = (resultStr.indexOf(faultStr) == -1);
			}
			else 
			{
				faultStr = "Error adding user.";
				isSuccess = (resultStr.indexOf(faultStr) == -1);
			}
			trace("[ServerManager] onResultGet: ", resultStr, isSuccess);
			
			if (isSuccess)
				_onSuccessCallback.call();
			else 
				_onFailCallback.call();
		}
	}
}