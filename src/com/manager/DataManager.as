package com.manager
{
	import com.abstract.FamilySession;
	
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;

	public class DataManager
	{
		public const MAX_SESSIONS:uint = 4;
		
		private var _userName:String;
		private var _userPassword:String;
		
		private var _curFingerNum:int = 1;
		private var _curFingerImage:BitmapData;
		private var _curHandNum:int;
		private var _curBackgroundNum:int;
		private var _curSavedVideoURL:String;
		private var _curSessionIndex:int;

		private var _familyMemberTypes:Array = [1, 2, 3, 4, 5];
		private var _familyMemberImages:Array = [null, null, null, null, null];
		private var _familyMemberVoices:Array = [null, null, null, null, null];

		private static var sharedObject:SharedObject = SharedObject.getLocal("FingerFamily");

		public var sessions:Array = [];// Of FamilySession

		public function DataManager()
		{
		}

		public function setFingerImage(value:BitmapData):void
		{
			_curFingerImage = value;
			_familyMemberImages[curFingerNum - 1] = value;
		}

		public function getFingerImage(fingerNum:int):BitmapData
		{
			if (_familyMemberImages[fingerNum])
				return _familyMemberImages[fingerNum];

			return null;
		}

		public function setFingerNum(value:int):void
		{
			_curFingerNum = value;
		}
		
		public function setFamilyMember(value:int):void
		{
			_familyMemberTypes[curFingerNum - 1] = value;
		}
		
		public function getFamilyMember(fingerNum:int):int
		{
			if (_familyMemberTypes[fingerNum - 1])
				return _familyMemberTypes[fingerNum - 1];

			return 1;
		}
		
		public function setFamilyMemberVoice(value:ByteArray):void
		{
			_familyMemberVoices[curFingerNum - 1] = value;
		}
		
		public function getFamilyMemberVoice(fingerNum:int):ByteArray
		{
			if (_familyMemberVoices[fingerNum - 1])
				return _familyMemberVoices[fingerNum - 1];
			
			return null;
		}
		
		public function setHand(value:int):void
		{
			_curHandNum = value;
			// Save to session
			var session:Object = sessions[sessions.length - 1];
			if (session) session.hand = _curHandNum;
		}
		
		public function setBackground(value:int):void
		{
			_curBackgroundNum = value;
			// Save to session
			var session:Object = sessions[sessions.length - 1];
			if (session) session.background = _curBackgroundNum;
		}
		
		public function set setSavedVideoURL(value:String):void
		{
			_curSavedVideoURL = value;
		}
		
		// get/set
		public function get userName():String { return _userName; }
		public function get userPassword():String { return _userPassword; }
		public function get curFingerNum():int { return _curFingerNum; }
		public function get curFingerImage():BitmapData { return _curFingerImage; }
		public function get curFamilyMember():int { return _familyMemberTypes[curFingerNum - 1]; }
		public function get curFamilyMemberVoice():ByteArray { return _familyMemberVoices[curFingerNum - 1]; }
		public function get curHandNum():int { return _curHandNum; }
		public function get curBackgroundNum():int { return _curBackgroundNum; }
		public function get curSavedVideoURL():String { return _curSavedVideoURL; }
		public function get isFingersSet():Boolean
		{
			var result:Boolean = true;
			for each (var image:BitmapData in _familyMemberImages)
			{
				if (!image)
				{
					result = false;
					break;
				}
			}
			return result;
		}

		public function get isVoicesSet():Boolean
		{
			var result:Boolean = true;
			for each (var audio:ByteArray in _familyMemberVoices)
			{
				if (!audio)
				{
					result = false;
					break;
				}
			}
			return result;
		}

		public function get isBackgroundSet():Boolean
		{
			return _curBackgroundNum != 0;
		}
		
		public function get isEmptyHand():Boolean
		{
			for each (var image:* in _familyMemberImages)
			{
				if (image != null)
					return false;
			}
			return true;
		}
		
		public function resetFamily():void
		{
			_curFingerNum = 1;
			_curFingerImage = null;
			_curHandNum = 0;
			_curBackgroundNum = 0;
			_curSavedVideoURL = "";
			
			_curSessionIndex = sessions.length;
			
			_familyMemberTypes = [1, 2, 3, 4, 5];
			_familyMemberImages = [null, null, null, null, null];
			_familyMemberVoices = [null, null, null, null, null];
		}
		
		public function saveUser(userName:String, userPassword:String):void
		{
			if (_userName != userName)
				_userName = userName;
			if (_userPassword != userPassword)
				_userPassword = userPassword;
			
			trace("Saving user...");
			sharedObject.data.userName = _userName;
			sharedObject.data.userPassword = _userPassword;
			sharedObject.flush();
			trace("User saved");
		}
		
		public function saveFamily():void
		{
			trace("Saving session...");

			var familyImages:Array = [];
			for each (var memberBitmapData:BitmapData in _familyMemberImages)
			{
				var imageObject:Object = {"byteArray":memberBitmapData.getPixels(memberBitmapData.rect),
					"width":memberBitmapData.rect.width, "height":memberBitmapData.rect.height};
				imageObject.byteArray.position = 0;

				familyImages.push(imageObject);
			}
			
			sessions[_curSessionIndex] = (new FamilySession(new Date().getTime(), familyImages, _familyMemberVoices,
					_familyMemberTypes, _curHandNum, _curBackgroundNum));
			if (sessions.length > MAX_SESSIONS) sessions.shift();// Limit number of sessions and remove oldest session
			
			sharedObject.data.sessions = sessions;
			sharedObject.flush();
			trace("Session saved");
		}

		public function loadFamily():void
		{
			trace("Loading previous sessions...");
			if (!sharedObject.data)
				return;
			
			if (sharedObject.data.userName)
			{
				_userName = sharedObject.data.userName;
				_userPassword = sharedObject.data.userPassword;
			}
			
			if (sharedObject.data.sessions)
			{
				sessions = sharedObject.data.sessions.concat();// Shallow clone of the array
				if (sessions && sessions.length > 0)
				{
					var session:Object = sessions[sessions.length - 1];// Get last session
					_familyMemberImages = [];
					for each (var object:Object in session.images)
					{
						var bitmapData:BitmapData = new BitmapData(object.width, object.height, false);
						bitmapData.setPixels(new Rectangle(0, 0, object.width, object.height), object.byteArray);
						_familyMemberImages.push(bitmapData);
					}

					_familyMemberVoices = session.voices;
					_familyMemberTypes = session.types;
					_curHandNum = session.hand;
					_curBackgroundNum = session.background;
					trace("Loaded");
				}
			}
		}

		public function openSession(index:uint):void
		{
			if (index > sessions.length)
				return;
			
			_curSessionIndex = index - 1;
			
			var session:Object = sessions[index - 1];
			trace("Opening session", index, "(" + new Date(session.time).toDateString() + " - " + new Date(session.time).toTimeString() + ")...");

			_familyMemberImages = [];
			for each (var object:Object in session.images)
			{
				var bitmapData:BitmapData = new BitmapData(object.width, object.height, false);
				object.byteArray.position = 0;
				bitmapData.setPixels(new Rectangle(0, 0, object.width, object.height), object.byteArray);
				_familyMemberImages.push(bitmapData);
			}

			_familyMemberVoices = session.voices;
			_familyMemberTypes = session.types;
			_curHandNum = session.hand;
			_curBackgroundNum = session.background;
			trace("Opened");
		}
		
		public function removeSession(index:int):void
		{
			trace("Removing session...");
			
			sessions.splice(index, 1);
			
			sharedObject.data.sessions = sessions;
			sharedObject.flush();
			trace("Session removed");
		}
		
		public function getCurSession():Object
		{
			return sessions[_curSessionIndex];
		}
	}
}