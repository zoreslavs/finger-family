package com.manager
{
	import com.abstract.IScreen;
	import com.data.PopupName;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;

	public class ScreenManager
	{
		private var _container:Sprite;
		private var _popupBackground:Sprite;
		private var _curScreen:IScreen;
		private var _curPopup:IScreen;
		private var _popupQueue:Array = [];
		
		public function ScreenManager(container:Sprite)
		{
			_container = container;
			
			var popupBG:Shape = new Shape();
			popupBG.graphics.clear() ;
			popupBG.graphics.beginFill(0x000000, 0.5);
			popupBG.graphics.drawRect(-AppSizeManager.DEFAULT_WIDTH * 0.5, 0, AppSizeManager.DEFAULT_WIDTH * 2, AppSizeManager.DEFAULT_HEIGHT);
			popupBG.graphics.endFill();
			_popupBackground = new Sprite();
			_popupBackground.addChild(popupBG);
		}
		
		public function showScreen(screenClass:Class):void
		{
			if (_curScreen == screenClass)
				throw Error("Screen " + screenClass + " is shown!");
			
			if (_curScreen != null)
			{
				_curScreen.dispose();
				_container.removeChild(_curScreen.content);
				_curScreen = null;
			}
			
			_curScreen = new screenClass();
			_curScreen.init();
			_container.addChild(_curScreen.content);
		}
		
		public function showPopup(popupClass:Class):void
		{
			if (_curPopup == popupClass)
				throw Error("Popup " + popupClass + " is shown!");
			
			_curPopup = new popupClass();
			_curPopup.init();
			if (popupClass == PopupName.LOADING_POPUP)
			{
				_curPopup.content.x = AppSizeManager.DEFAULT_WIDTH * 0.5;
				_curPopup.content.y = AppSizeManager.DEFAULT_HEIGHT * 0.5;
			}
			else 
			{
				_curPopup.content.x = AppSizeManager.DEFAULT_WIDTH * 0.5 - _curPopup.content.width * 0.5;
				_curPopup.content.y = AppSizeManager.DEFAULT_HEIGHT * 0.5 - _curPopup.content.height * 0.5;
			}
			
			if (!_popupQueue.length)
				_container.addChild(_popupBackground);
			else if (_popupBackground.parent)
				_container.setChildIndex(_popupBackground, _container.numChildren - 1);
			
			_container.addChild(_curPopup.content);
			_popupQueue.push(_curPopup);
		}
		
		public function hidePopup():void
		{
			if (!_popupQueue.length)
				return;
			
			_curPopup = _popupQueue.pop();
			if (_curPopup != null)
			{
				_curPopup.dispose();
				_container.removeChild(_curPopup.content);
				_curPopup = null;
			}
			
			if (!_popupQueue.length)
			{
				_container.removeChild(_popupBackground);
			}
			else 
			{
				//var index:int = _container.getChildIndex(_popupQueue[_popupQueue.length - 1].content);
				//_container.setChildIndex(_popupBackground, index);
				try 
				{
					var index:int = _container.getChildIndex(_popupQueue[_popupQueue.length - 1].content);
					_container.setChildIndex(_popupBackground, index);
				}
				catch (e:Error)
				{
					_container.removeChildren(1);
					_popupQueue = [];
				}
			}
		}
		
		// get/set
		public function get curScreen():IScreen { return _curScreen; }
		public function get curPopup():IScreen { return _curPopup; }
		public function get container():Sprite { return _container; }
	}
}