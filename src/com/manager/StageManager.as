package com.manager
{
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class StageManager extends EventDispatcher
	{
		private var _container:Sprite;
		private var _isActivated:Boolean = true;
		
		public function StageManager(container:Sprite)
		{
			_container = container;
			
			_container.stage.scaleMode = StageScaleMode.NO_SCALE;
			_container.stage.align = StageAlign.TOP_LEFT;
			_container.stage.color = 0xF5F8F8;
			
			//_container.stage.frameRate = 20;
			
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			_container.stage.addEventListener(Event.ACTIVATE, onActivate);
			_container.stage.addEventListener(Event.DEACTIVATE, onDeactivate);
			
			/*if (AndroidFullScreen.isSupported)
			{
				AndroidFullScreen.stage = _container.stage;
				AndroidFullScreen.fullScreen();
				_container.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			}*/
		}
		
		private function onActivate(event:Event):void
		{
			trace("Stage activated!");
			_isActivated = true;
		}
		
		private function onDeactivate(event:Event):void
		{
			trace("Stage deactivated!");
			_isActivated = false;
		}
		
		// get/set
		public function get isActivated():Boolean { return _isActivated; }
	}
}