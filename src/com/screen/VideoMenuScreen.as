package com.screen
{
	import com.Application;
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	public class VideoMenuScreen extends Screen
	{
		private var _content:VideoMenuScreenFLA;
		private var _marks:Array;
		
		public function VideoMenuScreen():void
		{
			_content = new VideoMenuScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_marks = [_content.mcMark1, _content.mcMark2];
			for each (var mcMark:Sprite in _marks) 
			{
				mcMark.visible = false;
			}
			
			_content.btnCreate.mouseEnabled = false;
			
			_content.btnPhotos.addEventListener(MouseEvent.CLICK, onPhotosClick);
			_content.btnBackgrounds.addEventListener(MouseEvent.CLICK, onBackgroundsClick);
			_content.btnCreate.addEventListener(MouseEvent.CLICK, onCreateClick);
			_content.btnBack.addEventListener(MouseEvent.CLICK, onBackClick);
			
			checkCreateEnabling();			
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnPhotos.removeEventListener(MouseEvent.CLICK, onPhotosClick);
			_content.btnBackgrounds.removeEventListener(MouseEvent.CLICK, onBackgroundsClick);
			_content.btnCreate.removeEventListener(MouseEvent.CLICK, onCreateClick);
			_content.btnBack.removeEventListener(MouseEvent.CLICK, onBackClick);
		}
		
		protected function onPhotosClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.SET_FINGERS_SCREEN);
		}
		
		protected function onBackgroundsClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.SET_BACKGROUND_SCREEN);
		}
		
		protected function onCreateClick(e:MouseEvent):void
		{
			app.screen.showPopup(PopupName.LOADING_POPUP);
			setTimeout(showFinalScreen, 200);
		}
		
		protected function onBackClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.MENU_SCREEN);
		}
		
		private function showFinalScreen():void
		{
			app.screen.showScreen(ScreenName.FINAL_SCREEN);
		}
		
		private function showMark(index:int):void
		{
			if (!_marks[index].visible)
				_marks[index].visible = true;
		}
		
		private function checkCreateEnabling():void
		{
			if (app.data.isFingersSet)
				showMark(0);
			
			if (app.data.isBackgroundSet)
				showMark(1);
			
			var isEnabled:Boolean = (app.data.isFingersSet && app.data.isBackgroundSet);
			_content.btnCreate.mouseEnabled = isEnabled;
			var frame:int = (isEnabled) ? 2 : 1;
			_content.btnCreate.gotoAndStop(frame);
		}
	}
}