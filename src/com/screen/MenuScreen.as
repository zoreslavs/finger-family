package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	
	import flash.events.MouseEvent;
	import com.data.ScreenName;
	
	public class MenuScreen extends Screen
	{
		private var _content:MenuScreenFLA;
		
		public function MenuScreen():void
		{
			_content = new MenuScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnPlay.addEventListener(MouseEvent.CLICK, onPlayClick);
			_content.btnCreate.addEventListener(MouseEvent.CLICK, onCreateClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnPlay.removeEventListener(MouseEvent.CLICK, onPlayClick);
			_content.btnCreate.removeEventListener(MouseEvent.CLICK, onCreateClick);
		}
		
		protected function onPlayClick(e:MouseEvent):void
		{
			if (app.data.sessions && app.data.sessions.length > 0)
				app.screen.showPopup(PopupName.SESSIONS_POPUP);
			else
				app.screen.showPopup(PopupName.NO_VIDEOS_POPUP);
		}
		
		protected function onCreateClick(e:MouseEvent):void
		{
			// reset current family config
			app.data.resetFamily();
			
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
		}
	}
}