package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.data.SoundData;
	import com.utils.AudioRecordHelper;
	import com.utils.PlatformCheckHelper;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.PermissionEvent;
	import flash.media.Microphone;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.permissions.PermissionStatus;
	import flash.utils.setTimeout;
	
	import avmplus.FLASH10_FLAGS;
	
	public class RecordVoiceScreen extends Screen
	{
		private static const START_RECORDING_FRAME:int = 45;
		private static const RECORDING_TIME:int = 3700;
		
		private static const BABY_FRAME_NUM:int = 3;
		private static const BABY_MASK_POSITION_X:int = 272;
		private static const OTHER_MASK_POSITION_X:int = 275;
		private static const MASK_POSITIONS_Y:Array = [196, 196, 279, 195, 193, 195, 193, 197, 197, 197, 197, 202, 202, 202, 196, 196, 196];
		
		private var _content:RecordVoiceScreenFLA;
		private var _audioRecorder:AudioRecordHelper;
		private var _musicSound:Sound;
		private var _musicChannel:SoundChannel = new SoundChannel();
		private var _numMembers:int;
		private var _isBtnPressed:Boolean;
		private var _isRecording:Boolean;
		private var _curFrame:int;
		
		public function RecordVoiceScreen():void
		{
			_content = new RecordVoiceScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_audioRecorder = new AudioRecordHelper(onAudioRecorded);
			_numMembers = _content.mcIcon.totalFrames;
			
			// for mic enabling
			playMuteMusic();
			
			_content.mcRunningText.gotoAndStop(1);
			_content.mcRunningText.mouseEnabled = _content.mcRunningText.mouseChildren = false;
			
			(app.data.curFamilyMember) ? setIconFrame(app.data.curFamilyMember) : setIconFrame(1);
			
			_content.btnRecord.addEventListener(MouseEvent.CLICK, onRecordClick);
			_content.btnSkip.addEventListener(MouseEvent.CLICK, onSkipClick);
			
			showMemberImage();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_audioRecorder = null;
			
			_content.btnRecord.removeEventListener(MouseEvent.CLICK, onRecordClick);
			_content.btnSkip.removeEventListener(MouseEvent.CLICK, onSkipClick);
		}
		
		private function showMemberImage():void
		{
			var image:Bitmap = new Bitmap(app.data.curFingerImage);
			image.smoothing = true;
			_content.mcImagePlace.addChild(image);
			
			if (_content.mcImagePlace.width < _content.mcImagePlace.height)
			{
				_content.mcImagePlace.width = _content.mcImageMask.width;
				_content.mcImagePlace.scaleY = _content.mcImagePlace.scaleX;
			}
			else 
			{
				_content.mcImagePlace.height = _content.mcImageMask.height;
				_content.mcImagePlace.scaleX = _content.mcImagePlace.scaleY;
			}
			_content.mcImagePlace.x = _content.mcImageMask.x;
			_content.mcImagePlace.y = _content.mcImageMask.y;
		}
		
		protected function onRecordClick(e:MouseEvent):void
		{
			//setRecording();
			if (PlatformCheckHelper.isAndroid && (Microphone.isSupported && Microphone.permissionStatus != PermissionStatus.GRANTED))
			{
				checkMicPermission(setRecording);
			}
			else 
			{
				setRecording();
			}
		}
		
		protected function onSkipClick(e:MouseEvent):void
		{
			//_audioRecorder.playRecording();
			if (_content.hasEventListener(Event.ENTER_FRAME))
				_content.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			if (_isRecording)
			{
				_audioRecorder.stopRecording();
				_audioRecorder = null;
			}
			
			showFingersScreen();
		}
		
		protected function onEnterFrame(e:Event):void
		{
			_curFrame++;
			if (_curFrame == START_RECORDING_FRAME)
			{
				startRecording();
			}
		}
		
		private function setRecording():void
		{
			if (_isBtnPressed)
				return;
			
			_isBtnPressed = true;
			_content.mcRunningText.play();
			
			_content.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function startRecording():void
		{
			if (_isRecording)
				return;
			
			_isRecording = true;
			
			_audioRecorder.startRecording();
			
			//playMusic();
			
			setTimeout(stopRecording, RECORDING_TIME);
		}
		
		private function stopRecording():void
		{
			if (!_audioRecorder)
				return;
			
			if (!_isRecording)
				return;
			
			_isRecording = false;
			if (_content.hasEventListener(Event.ENTER_FRAME))
				_content.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			_audioRecorder.stopRecording();
			onAudioRecorded();
			
			setTimeout(showFingersScreen, 1500);
		}
		
		private function playMuteMusic():void
		{
			_musicSound = new SoundData.MUSIC_HOW_DO_YOU_DO();
			var trans:SoundTransform = new SoundTransform(0);
			_musicSound.play(0, 0, trans);
		}
		
		private function playMusic(mute:Boolean = false):void
		{
			_musicSound = new SoundData.MUSIC_HOW_DO_YOU_DO_JOINED();
			_musicChannel = _musicSound.play();
			_musicChannel.addEventListener(Event.SOUND_COMPLETE, onMusicComplete);
		}
		
		private function onMusicComplete(e:Event):void
		{
			_musicChannel.removeEventListener(Event.SOUND_COMPLETE, onMusicComplete);
		}
		
		private function onAudioRecorded():void
		{
			app.data.setFamilyMemberVoice(_audioRecorder.getRecordingBytes());
		}
		
		private function showFingersScreen():void
		{
			app.screen.showScreen(ScreenName.SET_FINGERS_SCREEN);
		}
		
		private function setIconFrame(frame:int):void
		{
			_content.mcImageMask.x = (frame == 3) ? BABY_MASK_POSITION_X : OTHER_MASK_POSITION_X;
			_content.mcImageMask.y = MASK_POSITIONS_Y[frame - 1];
			_content.mcImagePlace.x = _content.mcImageMask.x;
			_content.mcImagePlace.y = _content.mcImageMask.y;
			
			_content.mcIcon.gotoAndStop(frame);
		}
		
		private function checkMicPermission(onPermissionGet:Function = null):void
		{
			if (Microphone.permissionStatus != PermissionStatus.GRANTED)
			{
				var mic:Microphone = Microphone.getMicrophone();
				mic.addEventListener(PermissionEvent.PERMISSION_STATUS, function(e:PermissionEvent):void 
				{
					if (e.status == PermissionStatus.GRANTED)
					{
						if (onPermissionGet)
							onPermissionGet.call();
					}
					else
					{
						app.screen.hidePopup();
						app.screen.showPopup(PopupName.MIC_PERMISSION_POPUP);
					}
				});
				mic.requestPermission();
			}
		}
	}
}