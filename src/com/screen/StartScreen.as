package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import flash.desktop.NativeApplication;
	import flash.events.MouseEvent;
	
	public class StartScreen extends Screen
	{
		private var _content:StartScreenFLA;
		
		public function StartScreen():void
		{
			_content = new StartScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			var appXML:XML =  NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXML.namespace();
			_content.txtVersion.text = "v." + appXML.ns::versionNumber;
			
			_content.btnLogin.addEventListener(MouseEvent.CLICK, onLoginClick);
			_content.btnNewAccount.addEventListener(MouseEvent.CLICK, onNewAccountClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnLogin.removeEventListener(MouseEvent.CLICK, onLoginClick);
			_content.btnNewAccount.removeEventListener(MouseEvent.CLICK, onNewAccountClick);
		}
		
		protected function onLoginClick(e:MouseEvent):void
		{
			if (!app.network.isConnected)
			{
				app.screen.showPopup(PopupName.CONNECTION_POPUP);
				return;
			}
			
			app.screen.showScreen(ScreenName.LOGIN_SCREEN);
		}
		
		protected function onNewAccountClick(e:MouseEvent):void
		{
			if (!app.network.isConnected)
			{
				app.screen.showPopup(PopupName.CONNECTION_POPUP);
				return;
			}
			
			app.screen.showScreen(ScreenName.NEW_ACCOUNT_SCREEN);
		}
	}
}