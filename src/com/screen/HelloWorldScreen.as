package com.screen
{
	import com.abstract.Screen;
	import com.rainbowcreatures.FWVideoEncoder;
	import com.utils.PlatformCheckHelper;
	import com.utils.VideoEnablingHelper;
	
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.StatusEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	
	public class HelloWorldScreen extends Screen
	{
		private static const VIDEO_SCALE:Number = 0.5;
		
		private var myEncoder:FWVideoEncoder;
		private var frameIndex:Number = 0;
		private var maxFrames:Number = 200;
		private var txt:TextField = new TextField();
		private var _content:MovieClip = new MovieClip();
		
		public function HelloWorldScreen()
		{
			super(_content);
			_content.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		override public function init():void
		{
			super.init();
			
			//app.sizeManager.setLandscapeSize();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		private function onAddedToStage(e:Event):void
		{
			var tf:TextFormat = new TextFormat("Arial", 60, 0XAA5050);
			txt.text = "Hello world!";
			txt.setTextFormat(tf);
			txt.width = 300;
			txt.y = _content.stage.fullScreenHeight * 0.5;
			_content.addChild(txt);
			
			_content.graphics.beginFill(0xffffff, 1);
			_content.graphics.drawRect(0, 0, _content.stage.stageWidth, _content.stage.stageHeight);
			_content.graphics.endFill();
			
			myEncoder = FWVideoEncoder.getInstance(_content);
			myEncoder.addEventListener(StatusEvent.STATUS, onStatus);
			myEncoder.load();
		}
		
		private function onStatus(e:StatusEvent):void
		{
			trace("Got status");
			if (e.code == "ready") {
				if (VideoEnablingHelper.isVideoRealtimeEnabled)
				{
					myEncoder.start(20, FWVideoEncoder.AUDIO_OFF);
				}
				else 
				{
					myEncoder.setDimensions(_content.stage.fullScreenWidth * VIDEO_SCALE, _content.stage.fullScreenHeight * VIDEO_SCALE);
					myEncoder.start(20, FWVideoEncoder.AUDIO_OFF, false);
				}
				
				trace("FlashyWrappers ready! Init...");
				_content.addEventListener(Event.ENTER_FRAME, onFrame);
			}
			if (e.code == "encoded") {
				var bo:ByteArray = myEncoder.getVideo();
				trace("Recording finished, video length: " + bo.length);                                
				
				myEncoder.saveToGallery();
				// save the file for check
				/*var saveFile:FileReference = new FileReference();
				saveFile.addEventListener(Event.COMPLETE, saveCompleteHandler);
				saveFile.addEventListener(IOErrorEvent.IO_ERROR, saveIOErrorHandler);
				saveFile.save(bo, "video.mp4");*/                             
			}
			if (e.code == "gallery_saved") {
				trace("Recording saved to gallery!");
			}
		}
		
		private function onFrame(e:Event):void
		{
			// animate the rectangle
			txt.x += 2;
			
			// clear the background, otherwise all transparency will be black by default in the video
			if (frameIndex % 10 == 0)
			{
				_content.graphics.beginFill(Math.random() * 0xffffff, 1);
				_content.graphics.drawRect(0, 0, _content.stage.stageWidth, _content.stage.stageHeight);
				_content.graphics.endFill();
			}
			
			// render one frame of your stuff into someMovieClip, then capture and
			// add one frame into the FlashyWrappers video encoder like this:
			
			if (VideoEnablingHelper.isVideoRealtimeEnabled)
			{
				myEncoder.capture();
			}
			else 
			{
				var bitmapData:BitmapData = new BitmapData(_content.stage.fullScreenWidth * VIDEO_SCALE, _content.stage.fullScreenHeight * VIDEO_SCALE);
				var bounds:Rectangle = new Rectangle(0, 0, bitmapData.width, bitmapData.height);
				var matrix:Matrix = new Matrix();
				matrix.scale(VIDEO_SCALE, VIDEO_SCALE);
				bitmapData.draw(_content.stage, matrix);
				myEncoder.addVideoFrame(bitmapData.getPixels(bounds));
			}
			
			frameIndex++;
			if (frameIndex >= maxFrames - 1) {
				_content.removeEventListener(Event.ENTER_FRAME, onFrame);                
				myEncoder.finish();
			}
		}                           
		
		// file was saved
		private function saveCompleteHandler(e:Event):void
		{
			trace("Video saved!");
		}
		
		// some error happened
		private function saveIOErrorHandler(e:IOErrorEvent):void
		{
			trace("Video NOT saved:(");
		}
	}
}