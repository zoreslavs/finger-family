package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.utils.TextCheckHelper;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class LoginScreen extends Screen
	{
		private const EMAIL_DEFAULT_TEXT:String = "Enter your email";
		private const PASSWORD_DEFAULT_TEXT:String = "Enter your password";
		
		private var _content:LoginScreenFLA;
		private var _defaultTextFormat:TextFormat;
		private var _fullTextFormat:TextFormat;
		
		public function LoginScreen():void
		{
			_content = new LoginScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_defaultTextFormat = _content.txtEmail.defaultTextFormat;
			_fullTextFormat = _content.txtEmail.defaultTextFormat;
			_fullTextFormat.color = "0x000000";
			
			/*_content.txtDevice.text = "Device info: \n";
			_content.txtDevice.text += NativeDeviceProperties.PRODUCT_MODEL.value + "\n";
			_content.txtDevice.text += NativeDeviceProperties.PRODUCT_BRAND.value + "\n";
			_content.txtDevice.text += NativeDeviceProperties.LCD_DENSITY.value + "\n";*/
			_content.removeChild(_content.txtDevice);
			
			if (app.data.userName)
			{
				_content.txtEmail.defaultTextFormat = _fullTextFormat;
				_content.txtEmail.text = app.data.userName;
			}
			else 
			{
				_content.txtEmail.text = "";
				showDefaultValue(_content.txtEmail, EMAIL_DEFAULT_TEXT);
			}
			
			if (app.data.userPassword)
			{
				_content.txtPassword.defaultTextFormat = _fullTextFormat;
				_content.txtPassword.text = app.data.userPassword;
			}
			else 
			{
				_content.txtPassword.text = "";
				showDefaultValue(_content.txtPassword, PASSWORD_DEFAULT_TEXT);
			}
			_content.txtEmail.addEventListener(FocusEvent.FOCUS_IN, onEmailFocusIn);
			_content.txtEmail.addEventListener(FocusEvent.FOCUS_OUT, onEmailFocusOut);
			_content.txtPassword.addEventListener(FocusEvent.FOCUS_IN, onPasswordFocusIn);
			_content.txtPassword.addEventListener(FocusEvent.FOCUS_OUT, onPasswordFocusOut);
			
			_content.btnContinue.addEventListener(MouseEvent.CLICK, onContinueClick);
			_content.btnBack.addEventListener(MouseEvent.CLICK, onBackClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.txtEmail.removeEventListener(FocusEvent.FOCUS_IN, onEmailFocusIn);
			_content.txtEmail.removeEventListener(FocusEvent.FOCUS_OUT, onEmailFocusOut);
			_content.txtPassword.removeEventListener(FocusEvent.FOCUS_IN, onPasswordFocusIn);
			_content.txtPassword.removeEventListener(FocusEvent.FOCUS_OUT, onPasswordFocusOut);
			
			_content.btnContinue.removeEventListener(MouseEvent.CLICK, onContinueClick);
			_content.btnBack.removeEventListener(MouseEvent.CLICK, onBackClick);
		}
		
		protected function onEmailFocusIn(e:FocusEvent):void
		{
			hideDefaultValue(_content.txtEmail, EMAIL_DEFAULT_TEXT);
		}
				
		protected function onEmailFocusOut(e:FocusEvent):void
		{
			showDefaultValue(_content.txtEmail, EMAIL_DEFAULT_TEXT);
		}
		
		protected function onPasswordFocusIn(e:FocusEvent):void
		{
			hideDefaultValue(_content.txtPassword, PASSWORD_DEFAULT_TEXT, true);
		}
		
		protected function onPasswordFocusOut(e:FocusEvent):void
		{
			showDefaultValue(_content.txtPassword, PASSWORD_DEFAULT_TEXT);
		}
		
		private function showDefaultValue(txt:TextField, defaultValue:String):void
		{
			if (txt.text == "")
			{
				txt.defaultTextFormat = _defaultTextFormat;
				txt.text = defaultValue;
				//txt.displayAsPassword = false;
			}
		}
		
		private function hideDefaultValue(txt:TextField, defaultValue:String, password:Boolean = false):void
		{
			if (txt.text == defaultValue)
			{
				txt.defaultTextFormat = _fullTextFormat;
				txt.text = "";
				//txt.displayAsPassword = password;
			}
		}
		
		private function onLoginSuccess():void
		{
			app.data.saveUser(userName, userPassword);
			app.screen.hidePopup();
			app.screen.showScreen(ScreenName.MENU_SCREEN);
		}
		
		private function onLoginFail():void
		{
			app.screen.hidePopup();
			app.screen.showPopup(PopupName.USER_NOT_EXISTS_POPUP);
		}
		
		private function onLoginError():void
		{
			app.screen.hidePopup();
			app.screen.showPopup(PopupName.SERVER_ERROR_POPUP);
		}
		
		protected function onContinueClick(e:MouseEvent):void
		{
			if (userName == EMAIL_DEFAULT_TEXT || TextCheckHelper.isEmptyText(userName))
			{
				app.screen.showPopup(PopupName.EMPTY_FIELDS_POPUP);
				return;
			}
			if (userPassword == PASSWORD_DEFAULT_TEXT || TextCheckHelper.isEmptyText(userPassword))
			{
				app.screen.showPopup(PopupName.EMPTY_FIELDS_POPUP);
				return;
			}
			if (!TextCheckHelper.isValidEmail(userName))
			{
				app.screen.showPopup(PopupName.INVALID_EMAIL_POPUP);
				return;
			}
			
			app.screen.showPopup(PopupName.LOADING_POPUP);
			app.server.loginUser(userName, userPassword, onLoginSuccess, onLoginFail, onLoginError);
			
			//app.server.loginUser("admin", "kissmyass1", onLoginSuccess, onLoginFail);
		}
		
		protected function onBackClick(e:MouseEvent):void
		{
			app.screen.showScreen(ScreenName.START_SCREEN);
		}
		
		// get/set
		private function get userName():String { return _content.txtEmail.text; }
		private function get userPassword():String { return _content.txtPassword.text; }
	}
}