package com.screen
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.utils.ImageZoomHelper;
	import com.utils.PlatformCheckHelper;
	import com.utils.VideoEnablingHelper;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	import flash.utils.clearTimeout;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	public class FinalScreen extends Screen
	{
		private static const NUM_FINGERS:int = 5;
		private static const MASK_SIZE:int = 100;
		private static const HAND_WAVE_TIME:int = 4000;
		private static const FINGER_ZOOM_TIME:int = 1000;
		private static const FINGER_SING_TIME:int = 4500;
		private static const TOTAL_ANIM_TIME:int = 4500 + (HAND_WAVE_TIME + FINGER_SING_TIME) * NUM_FINGERS * 2;
		private static const TOTAL_ANIM_FRAMES:int = Math.round(TOTAL_ANIM_TIME / 24);// - 225; //fps
		
		[Embed(source="../../../sound/music_empty.mp3")]
		private var _musicClass:Class;
		private var _musicSound:Sound;
		private var _musicChannel:SoundChannel;
		
		private var _content:FinalScreenFLA;
		private var _fingerIcons:Array;
		private var _fingerImages:Array = [];
		private var _fingerPositions:Array = [];
		private var _curFingerPos:Point;
		private var _numHandWaves:int;
		private var _handWaveTimeout:uint;
		private var _fingerZoomTimeout:uint;
		private var _fingerSingTimeout:uint;
		private var _startWaveTime:int;
		private var _loading:MovieClip;
		private var _loadingBG:Shape;
		
		public function FinalScreen():void
		{
			_content = new FinalScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			app.sizeManager.setLandscapeSize();
			
			_content.mcHandAnim.stop();
			
			_content.mcHandAnim.mcHand.mcHand.gotoAndStop(app.data.curHandNum);
			_content.mcBackground.gotoAndStop(app.data.curBackgroundNum);
			
			_content.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			with (_content.mcHandAnim.mcHand)
			{
				_fingerImages = [mcFingerImage1, mcFingerImage2, mcFingerImage3, mcFingerImage4, mcFingerImage5];
				for each (var mcFingerImage:Sprite in _fingerImages)
				{
					mcFingerImage.mouseEnabled = false;
				}
				_fingerIcons = [mcFingerIcon1, mcFingerIcon2, mcFingerIcon3, mcFingerIcon4, mcFingerIcon5];
				for each (var mcFingerIcon:Sprite in _fingerIcons)
				{
					mcFingerIcon.visible = false;
					mcFingerIcon.mouseEnabled = false;
				}
			}
			
			//_content.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			//_content.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			
			showFingerImages();
			recordVideo();
			
			// hide loading
			app.screen.hidePopup();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			_content.stage.quality = StageQuality.HIGH;
			
			_fingerIcons = [];
			_fingerImages = [];
		}
		
		protected function onAddedToStage(e:Event):void
		{
			_content.stage.quality = StageQuality.LOW;
			
			// align hand in the center of the screen (only for phones)
			if (!PlatformCheckHelper.isPhone)
				return;
			
			_content.mcHandAnim.x = (_content.stage.fullScreenHeight * 0.5 + 50) / _content.parent.scaleX;
		}
		
		private function startAnimation():void
		{
			//_startWaveTime = getTimer();
			//_handWaveTimeout = (_numHandWaves == 0) ? setTimeout(stopHandWave, HAND_WAVE_TIME * 2) : setTimeout(stopHandWave, HAND_WAVE_TIME);
			_content.mcHandAnim.play();
		}
		
		private function finishAnimation():void
		{
			_content.mcHandAnim.play();
			_numHandWaves = 0;
			setTimeout(stopAnimation, 5000);
		}
		
		private function stopAnimation():void
		{
		}
		
		private function stopHandWave():void
		{
			/*if (getTimer() - _startWaveTime < HAND_WAVE_TIME)
				return;
			
			if (_handWaveTimeout)
			{
				clearTimeout(_handWaveTimeout);
				_handWaveTimeout = 0;
			}*/
			
			_curFingerPos = _fingerPositions[_numHandWaves];
			_numHandWaves++;
			
			_content.mcHandAnim.gotoAndStop(1);
			
			if (_numHandWaves < NUM_FINGERS)
			{
				showFingerZoom();
				//_fingerSingTimeout = setTimeout(startAnimation, FINGER_SING_TIME);
			}
			else 
			{
				showFingerZoom();
				_fingerSingTimeout = setTimeout(finishAnimation, FINGER_SING_TIME);
			}
		}
		
		private function showFingerZoom():void
		{
			setTimeout(setHandAlpha, FINGER_ZOOM_TIME, 0.5);
			
			_fingerZoomTimeout = setTimeout(zoomOutFinger, FINGER_SING_TIME - FINGER_ZOOM_TIME);
			ImageZoomHelper.zoomImage(_content, _curFingerPos, FINGER_ZOOM_TIME / 1000, true);
		}
		
		private function zoomOutFinger():void
		{
			setTimeout(setHandAlpha, FINGER_ZOOM_TIME, 1);
			
			if (_fingerZoomTimeout)
			{
				clearTimeout(_fingerZoomTimeout);
				_fingerZoomTimeout = 0;
			}
			ImageZoomHelper.zoomImage(_content, _curFingerPos, FINGER_ZOOM_TIME / 1000, false);
		}
		
		private function showFingerImages():void
		{
			var numImages:int;
			for (var i:int = 0; i < NUM_FINGERS; i++)
			{
				var bd:BitmapData = app.data.getFingerImage(i);
				if (bd)
				{
					numImages++;
					showFingerImage(bd, i);
				}
			}
		}
		
		private function setHandAlpha(value:Number):void
		{
			if (!_fingerImages.length)
				return;
			
			_content.mcHandAnim.mcHand.mcHand.alpha = value;
			
			for (var i:int = 0; i < NUM_FINGERS; i++)
			{
				_fingerIcons[i].alpha = value;
				_fingerImages[i].alpha = value;
			}
			
			if (_numHandWaves == 0)
				return;
			
			var oppValue:Number = (value == 1) ? 0.5 : 1;
			if (oppValue == 1)
			{
				_fingerIcons[_numHandWaves - 1].alpha = oppValue;
				_fingerImages[_numHandWaves - 1].alpha = oppValue;
			}
		}
		
		private function showFingerImage(bd:BitmapData, index:int):void
		{
			var imageContainer:MovieClip = _fingerImages[index];
			var image:Bitmap = new Bitmap(bd);
			image.x = -image.width * 0.5;
			image.y = -image.height * 0.5;
			image.smoothing = true;
			imageContainer.mcImagePlace.addChild(image);
			
			if (imageContainer.mcImagePlace.width < imageContainer.mcImagePlace.height)
			{
				imageContainer.mcImagePlace.width = MASK_SIZE;
				imageContainer.mcImagePlace.scaleY = imageContainer.mcImagePlace.scaleX;
			}
			else 
			{
				imageContainer.mcImagePlace.height = MASK_SIZE;
				imageContainer.mcImagePlace.scaleX = imageContainer.mcImagePlace.scaleY;
			}
			imageContainer.mcImagePlace.cacheAsBitmap = true;
			imageContainer.mcImagePlace.mask = imageContainer.mcImageMask;
			
			var cloneData:BitmapData = new BitmapData(imageContainer.width, imageContainer.height, true, 0x00FFFFFF);
			var matrix:Matrix = new Matrix(1, 0, 0, 1, imageContainer.width * 0.5, imageContainer.height * 0.5);
			cloneData.draw(imageContainer, matrix);
			var clone:Bitmap = new Bitmap(cloneData);
			clone.smoothing = true;
			clone.x = -clone.width * 0.5;
			clone.y = -clone.height * 0.5;
			imageContainer.addChild(clone);
			
			imageContainer.removeChild(imageContainer.mcImagePlace);
			imageContainer.removeChild(imageContainer.mcImageMask);
			
			var curIcon:MovieClip = _fingerIcons[index];
			curIcon.gotoAndStop(app.data.getFamilyMember(index + 1));
			curIcon.visible = true;
			
			setZoomPosition(imageContainer, index);
		}
		
		private function setZoomPosition(imageContainer:Sprite, index:int):void
		{
			var position:Point = imageContainer.localToGlobal(new Point(0, 0));
			position.x -= 50;
			position.y += 50;
			_fingerPositions.push(position);
		}
		
		private function recordVideo():void
		{
			_musicSound = new _musicClass();
			_musicChannel = new SoundChannel();
			var soundBytes:ByteArray = new ByteArray();
			
			/*var sound:Sound = new _musicClass();
			var soundBytes:ByteArray = new ByteArray();
			sound.loadCompressedDataFromByteArray(soundBytes, soundBytes.length);
			sound.play();*/
			
			app.videoHelper.init(_content.mcHandAnim, TOTAL_ANIM_FRAMES, soundBytes, _musicSound, onAudioReady, onHandAnim, onZoomAnim, onRepeatAnim, onVideoStart, onVideoFinish, onVideoEncoded, onVideoFail);
		}
		
		private function onAudioReady():void
		{
			if (VideoEnablingHelper.isVideoEnabled)
				app.videoHelper.recordVideo();
		}
		
		private function onHandAnim():void
		{
			startAnimation();
		}
		
		private function onZoomAnim():void
		{
			stopHandWave();
		}
		
		private function onRepeatAnim():void
		{
			setTimeout(startAnimation, 500);
		}
		
		private function onVideoStart():void
		{
			setTimeout(startAnimation, 500);
		}
		
		private function onVideoFinish():void
		{
			if (VideoEnablingHelper.isVideoEnabled)
			{
				showLoading();
			}
			else 
			{
				app.screen.hidePopup();
				app.sizeManager.setPortraiteSize();
				
				app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
				app.screen.showPopup(PopupName.SAMSUNG7_VIDEO_POPUP);
			}
		}
		
		private function onVideoEncoded():void
		{
			if (VideoEnablingHelper.isVideoEnabled)
				app.videoHelper.saveVideoToFile(onVideoSaved);
		}
		
		private function onVideoFail():void
		{
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
		}
		
		private function showLoading():void
		{
			if (_loading)
				return;
			
			_loadingBG = new Shape();
			_loadingBG.graphics.clear() ;
			_loadingBG.graphics.beginFill(0x000000, 0.5);
			_loadingBG.graphics.drawRect(0, 0, _content.stage.fullScreenWidth, _content.stage.fullScreenHeight);
			_loadingBG.graphics.endFill();
			_content.stage.addChild(_loadingBG);
			
			_loading = new McSavingAnimationFLA();
			_loading.x = _content.stage.fullScreenWidth * 0.5;
			_loading.y = _content.stage.fullScreenHeight * 0.5;
			_content.stage.addChild(_loading);
		}
		
		private function onVideoSaved(url:String = null):void
		{
			trace("onVideoSaved: ", url);
			app.data.setSavedVideoURL = url;
			
			if (_loading)
			{
				_loadingBG.parent.removeChild(_loadingBG);
				_loading.parent.removeChild(_loading);
				_loadingBG = null;
				_loading = null;
			}
			
			app.screen.hidePopup();
			app.sizeManager.setPortraiteSize();
			
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
			app.screen.showPopup(PopupName.VIDEO_READY_POPUP);
		}
		
		protected function onMouseDown(event:MouseEvent):void
		{
			ImageZoomHelper.zoomImage(_content, new Point(_content.mouseX, _content.mouseY), 1, true);
		}
		
		protected function onMouseUp(event:MouseEvent):void
		{
			ImageZoomHelper.zoomImage(_content, new Point(_content.mouseX, _content.mouseY), 1, false);
		}
	}
}