package com.screen
{
	import com.abstract.Screen;
	import com.data.ScreenName;
	import flash.events.MouseEvent;
	
	public class SetBackgroundScreen extends Screen
	{
		private var _content:SetBackgroundScreenFLA;
		private var _numBackgrounds:int;
		
		public function SetBackgroundScreen():void
		{
			_content = new SetBackgroundScreenFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_numBackgrounds = _content.mcIcon.totalFrames;
			
			if (app.data.isBackgroundSet)
				_content.mcIcon.gotoAndStop(app.data.curBackgroundNum);
			else 
				_content.mcIcon.gotoAndStop(1);
			
			_content.btnLeft.addEventListener(MouseEvent.CLICK, onLeftClick);
			_content.btnRight.addEventListener(MouseEvent.CLICK, onRightClick);
			_content.btnDone.addEventListener(MouseEvent.CLICK, onDoneClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnLeft.removeEventListener(MouseEvent.CLICK, onLeftClick);
			_content.btnRight.removeEventListener(MouseEvent.CLICK, onRightClick);
			_content.btnDone.removeEventListener(MouseEvent.CLICK, onDoneClick);
		}
		
		protected function onLeftClick(e:MouseEvent):void
		{
			var frame:int = _content.mcIcon.currentFrame - 1;
			if (frame < 1)
				frame = _numBackgrounds;
			_content.mcIcon.gotoAndStop(frame);
		}
		
		protected function onRightClick(e:MouseEvent):void
		{
			var frame:int = _content.mcIcon.currentFrame + 1;
			if (frame > _numBackgrounds)
				frame = 1;
			_content.mcIcon.gotoAndStop(frame);
		}
		
		protected function onDoneClick(e:MouseEvent):void
		{
			app.data.setBackground(_content.mcIcon.currentFrame);
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
		}
	}
}