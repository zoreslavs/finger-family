package com.popup
{
	import com.abstract.Screen;
	
	import flash.events.MouseEvent;
	
	public class UserExistsPopup extends Screen
	{
		private var _content:UserExistsPopupFLA;
		
		public function UserExistsPopup():void
		{
			_content = new UserExistsPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
	}
}