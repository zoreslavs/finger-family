package com.popup
{
	import com.abstract.Screen;
	
	import flash.events.MouseEvent;
	
	public class Samsung7VideoPopup extends Screen
	{
		private var _content:Samsung7VideoPopupFLA;
		
		public function Samsung7VideoPopup():void
		{
			_content = new Samsung7VideoPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
	}
}