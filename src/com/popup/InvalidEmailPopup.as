package com.popup
{
	import com.abstract.Screen;
	
	import flash.events.MouseEvent;
	
	public class InvalidEmailPopup extends Screen
	{
		private var _content:InvalidEmailPopupFLA;
		
		public function InvalidEmailPopup():void
		{
			_content = new InvalidEmailPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
	}
}