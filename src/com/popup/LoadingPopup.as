package com.popup
{
	import com.abstract.Screen;
	
	public class LoadingPopup extends Screen
	{
		private var _content:LoadingAnimation;
		
		public function LoadingPopup():void
		{
			_content = new LoadingAnimation();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
	}
}