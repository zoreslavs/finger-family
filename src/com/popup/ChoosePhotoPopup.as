package com.popup
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.utils.ImagePickHelper;
	import com.utils.PlatformCheckHelper;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.events.PermissionEvent;
	import flash.media.CameraRoll;
	import flash.media.CameraUI;
	import flash.permissions.PermissionStatus;
	
	public class ChoosePhotoPopup extends Screen
	{
		private var _content:ChoosePhotoPopupFLA;
		
		public function ChoosePhotoPopup():void
		{
			_content = new ChoosePhotoPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnClose.addEventListener(MouseEvent.CLICK, onCloseClick);
			_content.btnTakePhoto.addEventListener(MouseEvent.CLICK, onTakePhotoClick);
			_content.btnGallery.addEventListener(MouseEvent.CLICK, onGalleryClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnClose.removeEventListener(MouseEvent.CLICK, onCloseClick);
			_content.btnTakePhoto.removeEventListener(MouseEvent.CLICK, onTakePhotoClick);
			_content.btnGallery.removeEventListener(MouseEvent.CLICK, onGalleryClick);
		}
		
		protected function onCloseClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
		
		protected function onTakePhotoClick(e:MouseEvent = null):void
		{
			//showCamera();
			if (PlatformCheckHelper.isAndroid && (CameraUI.isSupported && CameraUI.permissionStatus != PermissionStatus.GRANTED))
			{
				checkCameraPermission(showCamera);
			}
			else 
			{
				showCamera();
			}
		}
		
		protected function onGalleryClick(e:MouseEvent):void
		{
			//showGallery();
			if (PlatformCheckHelper.isAndroid && (CameraRoll.supportsBrowseForImage && CameraRoll.permissionStatus != PermissionStatus.GRANTED))
			{
				checkGalleryPermission(showGallery);
			}
			else 
			{
				showGallery();
			}
		}
		
		private function showCamera():void
		{
			app.screen.hidePopup();
			app.screen.showPopup(PopupName.LOADING_POPUP);
			ImagePickHelper.takePhoto(onImageGot, onCancel);
		}
		
		private function showGallery():void
		{
			app.screen.hidePopup();
			app.screen.showPopup(PopupName.LOADING_POPUP);
			ImagePickHelper.pickImage(onImageGot, onCancel);
		}
		
		private function onImageGot(image:BitmapData):void
		{
			app.screen.hidePopup();
			
			if (!image)
				return;
			
			app.data.setFingerImage(image);
			app.screen.showScreen(ScreenName.SET_PHOTO_SCREEN);
		}
		
		private function onCancel():void
		{
			app.screen.hidePopup();
		}
		
		private function checkCameraPermission(onPermissionGet:Function = null):void
		{
			if (!CameraUI.isSupported)
				return;
			
			if (CameraUI.permissionStatus != PermissionStatus.GRANTED)
			{
				var cam:CameraUI = new CameraUI();
				cam.addEventListener(PermissionEvent.PERMISSION_STATUS, function(e:PermissionEvent):void 
				{
					if (e.status == PermissionStatus.GRANTED)
					{
						if (onPermissionGet)
							onPermissionGet.call();
					}
					else
					{
						app.screen.hidePopup();
						app.screen.showPopup(PopupName.CAMERA_PERMISSION_POPUP);
					}
				});
				cam.requestPermission();
			}
		}
		
		private function checkGalleryPermission(onPermissionGet:Function = null):void
		{
			if (CameraRoll.permissionStatus != PermissionStatus.GRANTED)
			{
				var cam:CameraRoll = new CameraRoll();
				cam.addEventListener(PermissionEvent.PERMISSION_STATUS, function(e:PermissionEvent):void 
				{
					if (e.status == PermissionStatus.GRANTED)
					{
						if (onPermissionGet)
							onPermissionGet.call();
					}
					else
					{
						app.screen.hidePopup();
						app.screen.showPopup(PopupName.GALLERY_PERMISSION_POPUP);
					}
				});
				cam.requestPermission();
			}
		}
	}
}