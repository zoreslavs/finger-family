package com.popup
{
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.utils.TimeUtil;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class SessionsPopup extends Screen
	{
		public static const DAY:int = 24 * 3600000;
		
		private var _content:SessionsPopupFLA;
		private var _sessions:Array = [];
		private var _timerSessions:Array = [];
		private var _sessionsTime:Array = [];
		private var _timer:Timer;
		private var _isNoSessions:Boolean;

		public function SessionsPopup():void
		{
			_content = new SessionsPopupFLA();
			super(_content);
		}

		override public function init():void
		{
			super.init();
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
			
			_isNoSessions = false;
			updateSessions();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			disposeSessions();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		private function updateSessions(withDispose:Boolean = false):void
		{
			if (withDispose)
				disposeSessions();
			
			var date:Date;
			var hours:Number;
			for (var i:int; i < app.data.MAX_SESSIONS; i++)
			{
				var mcSession:MovieClip = _content["btnSession" + (i + 1)];
				if (app.data.sessions.length > i)
				{
					date = new Date(app.data.sessions[i].time);
					hours = date.getHours();
					// Formatting to 11/11/2016 - 6:00am
					var minutesStr:String = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes().toString();
					mcSession.txtDate.text = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
					mcSession.txtTime.text = (hours > 12 ? (hours - 12) : (hours == 0 ? 12 : hours)) + ":" + minutesStr + (hours < 12 ? "am" : "pm");
					mcSession.addEventListener(MouseEvent.CLICK, onSessionClick);
					mcSession.visible = true;
					
					if (!app.data.sessions[i].videoTime)
					{
						mcSession.txtExpires.visible = false;
						mcSession.txtExpiresTime.visible = false;
					}
					else 
					{
						if (!checkExpiration(mcSession, app.data.sessions[i].videoTime))
						{
							_sessionsTime.push(app.data.sessions[i].videoTime);
							_timerSessions.push(mcSession);
						}
						else 
						{
							break;
						}
					}
				}
				else
				{
					mcSession.visible = false;
				}
				_sessions.push(mcSession);
			}
			
			if (_timerSessions.length > 0)
			{
				_timer = new Timer(1000);
				_timer.addEventListener(TimerEvent.TIMER, onTimerTick);
				_timer.start();
			}
		}
		
		private function disposeSessions():void
		{
			_sessions = [];
			_timerSessions = [];
			_sessionsTime = [];
			
			if (_timer)
			{
				_timer.stop();
				_timer.removeEventListener(TimerEvent.TIMER, onTimerTick);
				_timer = null;
			}
			
			for (var i:int; i < app.data.MAX_SESSIONS; i++)
			{
				_content["btnSession" + (i + 1)].removeEventListener(MouseEvent.CLICK, onSessionClick);
			}
		}
		
		private function removeSession(mcSession:MovieClip):void
		{
			app.data.removeSession(_sessions.indexOf(mcSession));
			
			if (app.data.sessions.length > 0)
			{
				updateSessions(true);
			}
			else if (!_isNoSessions)
			{
				_isNoSessions = true;
				app.screen.hidePopup();
				app.screen.showPopup(PopupName.NO_VIDEOS_POPUP);
			}
		}
		
		private function onSessionClick(e:MouseEvent):void
		{
			//removeSession(e.currentTarget as MovieClip);
			
			var index:int = e.currentTarget.name.replace(/.*\D/g, "");
			if (index > 0) app.data.openSession(index);
			app.screen.hidePopup();
			app.screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
		}
		
		private function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
		
		private function onTimerTick(e:TimerEvent):void
		{
			for (var i:int = 0; i < _timerSessions.length; i++)
			{
				if (_timerSessions[i].visible)
				{
					checkExpiration(_timerSessions[i], _sessionsTime[i]);
				}
			}
		}
		
		private function checkExpiration(mcSession:MovieClip, startTime:Number):Boolean
		{
			var timeLeft:Number = DAY - Math.abs(startTime - new Date().time);
			if (timeLeft < 0)
			{
				removeSession(mcSession);
				return true;
			}
			else 
			{
				mcSession.txtExpiresTime.text = TimeUtil.timeParse(timeLeft);
			}
			return false;
		}
	}
}