package com.popup
{
	import com.abstract.Screen;
	
	import flash.events.MouseEvent;
	
	public class FacebookInstalledPopup extends Screen
	{
		private var _content:InstalledAppPopupFLA;
		
		public function FacebookInstalledPopup():void
		{
			_content = new InstalledAppPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.gotoAndStop(1);
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
	}
}