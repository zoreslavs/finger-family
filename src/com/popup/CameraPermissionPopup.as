package com.popup
{
	import com.abstract.Screen;
	
	import flash.events.MouseEvent;
	
	public class CameraPermissionPopup extends Screen
	{
		private var _content:PermissionsPopupFLA;
		
		public function CameraPermissionPopup():void
		{
			_content = new PermissionsPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.gotoAndStop(1);
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
	}
}