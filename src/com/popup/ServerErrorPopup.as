package com.popup
{
	import com.abstract.Screen;
	
	import flash.events.MouseEvent;
	
	public class ServerErrorPopup extends Screen
	{
		private var _content:ServerErrorPopupFLA;
		
		public function ServerErrorPopup():void
		{
			_content = new ServerErrorPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
	}
}