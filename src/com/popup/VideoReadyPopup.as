package com.popup
{
	import com.Application;
	import com.abstract.Screen;
	import com.data.PopupName;
	import com.utils.InstalledAppCheckHelper;
	import com.utils.PlatformCheckHelper;
	import com.utils.SocialMediaHelper;
	
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;
	
	public class VideoReadyPopup extends Screen
	{
		private var _content:VideoReadyPopupFLA;
		private var _socialHelper:SocialMediaHelper;
		private var _isSavePressed:Boolean;
		
		public function VideoReadyPopup():void
		{
			_content = new VideoReadyPopupFLA();
			super(_content);
		}
		
		override public function init():void
		{
			super.init();
			
			_socialHelper = new SocialMediaHelper();
			
			_content.btnGalleryAndroid.visible = false;
			_content.btnGalleryIOS.visible = false;
			_content.btnFacebookIOS.visible = false;
			_content.btnInstagramIOS.visible = false;
			
			_content.mcWarning.mouseEnabled = false;
			_content.mcWarning.visible = false;
			
			if (!app.data.getCurSession().videoTime)
			{
				app.data.getCurSession().videoTime = new Date().time;
				
				_content.btnSaveAndShare.y -= 40;
				_content.mcWarning.visible = true;
			}
			
			if (Application.DEBUG_MODE)
				onPurchaseSuccess();
			
			if (PlatformCheckHelper.isIOS)
			{
				_content.btnGalleryIOS.addEventListener(MouseEvent.CLICK, onGalleryClick);
				_content.btnFacebookIOS.addEventListener(MouseEvent.CLICK, onFacebookClick);
				_content.btnInstagramIOS.addEventListener(MouseEvent.CLICK, onInstagramClick);
			}
			else 
			{
				_content.btnGalleryAndroid.addEventListener(MouseEvent.CLICK, onGalleryClick);
			}
			_content.btnSaveAndShare.addEventListener(MouseEvent.CLICK, onSaveClick);
			_content.btnOK.addEventListener(MouseEvent.CLICK, onOKClick);
			
			_content.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			//disableButton(_content.btnOK);
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			if (PlatformCheckHelper.isIOS)
			{
				_content.btnGalleryIOS.removeEventListener(MouseEvent.CLICK, onGalleryClick);
				_content.btnFacebookIOS.removeEventListener(MouseEvent.CLICK, onFacebookClick);
				_content.btnInstagramIOS.removeEventListener(MouseEvent.CLICK, onInstagramClick);
			}
			else 
			{
				_content.btnGalleryAndroid.removeEventListener(MouseEvent.CLICK, onGalleryClick);
			}
			_content.btnSaveAndShare.removeEventListener(MouseEvent.CLICK, onSaveClick);
			_content.btnOK.removeEventListener(MouseEvent.CLICK, onOKClick);
			
			_content.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_content.stage.removeEventListener(Event.ACTIVATE, onStageActivate);
			_content.stage.removeEventListener(Event.DEACTIVATE, onStageDeactivate);
		}
		
		protected function onGalleryClick(e:MouseEvent):void
		{
			app.screen.showPopup(PopupName.LOADING_POPUP);
			
			(PlatformCheckHelper.isIOS) ? disableButton(_content.btnGalleryIOS) : disableButton(_content.btnGalleryAndroid);
			app.videoHelper.saveVideoToGallery(onVideoSaved);
			//AirImagePicker.getInstance().displayImagePicker(onVideoPicked, true);
		}
		
		protected function onFacebookClick(e:MouseEvent):void
		{
			if (!app.network.isConnected)
			{
				app.screen.showPopup(PopupName.CONNECTION_POPUP);
				return;
			}
			
			if (!InstalledAppCheckHelper.isFacebookAppInstalled)
			{
				app.screen.showPopup(PopupName.FB_INSTALLED_POPUP);
				return;
			}
			
			if (!app.data.curSavedVideoURL)
				return;
			
			app.screen.showPopup(PopupName.LOADING_POPUP);
			setTimeout(onStageActivate, 7500);
			
			disableButton(_content.btnFacebookIOS);
			//enableButton(_content.btnOK);
			_socialHelper.shareFacebook(app.data.curSavedVideoURL);
		}
		
		protected function onInstagramClick(e:MouseEvent):void
		{
			if (!app.network.isConnected)
			{
				app.screen.showPopup(PopupName.CONNECTION_POPUP);
				return;
			}
			
			if (!InstalledAppCheckHelper.isInstagramAppInstalled)
			{
				app.screen.showPopup(PopupName.INSTAGRAM_INSTALLED_POPUP);
				return;
			}
			
			if (!app.data.curSavedVideoURL)
				return;
			
			app.screen.showPopup(PopupName.LOADING_POPUP);
			setTimeout(onStageActivate, 7500);
			
			disableButton(_content.btnInstagramIOS);
			//enableButton(_content.btnOK);
			_socialHelper.shareInstagram(app.data.curSavedVideoURL);
		}
		
		protected function onSaveClick(e:MouseEvent):void
		{
			if (_isSavePressed)
				return;
			
			if (app.network.isConnected)
			{
				_isSavePressed = true;
				app.screen.showPopup(PopupName.LOADING_POPUP);
				app.store.makePurchase(onPurchaseSuccess, onPurchaseFail, onPurchaseCancel);
			}
			else 
			{
				_isSavePressed = false;
				app.screen.showPopup(PopupName.CONNECTION_POPUP);
			}
		}
		
		protected function onOKClick(e:MouseEvent):void
		{
			app.screen.hidePopup();
		}
		
		protected function onAddedToStage(e:Event):void
		{
			_content.stage.addEventListener(Event.ACTIVATE, onStageActivate);
			_content.stage.addEventListener(Event.DEACTIVATE, onStageDeactivate);
		}
		
		protected function onStageActivate(e:Event = null):void
		{
			if (!_content.btnSaveAndShare.visible && app.screen.curPopup is LoadingPopup)
				app.screen.hidePopup();
		}
		
		protected function onStageDeactivate(e:Event):void
		{
		}
		
		private function onPurchaseSuccess():void
		{
			if (app.screen.curPopup is LoadingPopup)
				app.screen.hidePopup();
			
			_isSavePressed = false;
			
			if (_content.mcWarning.visible)
				_content.mcWarning.visible = false;
			
			_content.btnSaveAndShare.visible = false;
			if (PlatformCheckHelper.isIOS)
			{
				_content.btnGalleryIOS.visible = true;
				_content.btnFacebookIOS.visible = true;
				_content.btnInstagramIOS.visible = true;
			}
			else 
			{
				_content.btnGalleryAndroid.visible = true;
			}
		}
		
		private function onPurchaseFail():void
		{
			if (app.screen.curPopup is LoadingPopup)
				app.screen.hidePopup();
			
			_isSavePressed = false;
			app.screen.showPopup(PopupName.PURCHASE_FAILED_POPUP);
		}
		
		private function onPurchaseCancel():void
		{
			if (app.screen.curPopup is LoadingPopup)
				app.screen.hidePopup();
			
			_isSavePressed = false;
		}
		
		private function onVideoPicked(status:String, url:String = "", data:BitmapData = null, bytes:ByteArray = null):void
		{
			trace("onVideoPicked: ", status, url);
		}
		
		private function onVideoSaved():void
		{
			trace("onVideoSavedToGallery");
			app.screen.hidePopup();
			app.screen.showPopup(PopupName.VIDEO_SAVED_POPUP);
			//enableButton(_content.btnOK);
		}
		
		private function disableButton(btn:MovieClip):void
		{
			if (!btn.mouseEnabled)
				return;
			
			btn.mouseEnabled = false;
			btn.alpha = 0.5;
		}
		
		private function enableButton(btn:MovieClip):void
		{
			if (btn.mouseEnabled)
				return;
			
			btn.mouseEnabled = true;
			btn.alpha = 1;
		}
	}
}