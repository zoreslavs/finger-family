package com
{
	import com.abstract.INetworkStatusHelper;
	import com.data.PopupName;
	import com.data.ScreenName;
	import com.manager.AppSizeManager;
	import com.manager.DataManager;
	import com.manager.ScreenManager;
	import com.manager.ServerManager;
	import com.manager.StageManager;
	import com.store.Store;
	import com.utils.AndroidDeviceHelper;
	import com.utils.NetworkStatusURLHelper;
	import com.utils.PlatformCheckHelper;
	import com.utils.VideoConvertionHelper;
	
	import flash.display.Sprite;
	import flash.utils.setTimeout;
	
	public class Application
	{
		public static const DEBUG_MODE:Boolean = false;
		
		private static var _instance:Application;
		
		public static function get instance():Application
		{
			if (!_instance)
				_instance = new Application();
			
			return _instance;
		}
		
		private var _container:Sprite;
		private var _stageManager:StageManager;
		private var _sizeManager:AppSizeManager;
		private var _dataManager:DataManager;
		private var _screenManager:ScreenManager;
		private var _serverManager:ServerManager;
		private var _storeManager:Store;
		private var _videoHelper:VideoConvertionHelper;
		private var _networkHelper:INetworkStatusHelper;
		
		public function init(container:Sprite):void
		{
			_container = container;
			
			PlatformCheckHelper.checkPlatform(_container);
			
			if (PlatformCheckHelper.isAndroid)
				AndroidDeviceHelper.checkDevice();
			
			_stageManager = new StageManager(container);
			_sizeManager = new AppSizeManager(container);
			_dataManager = new DataManager();
			_screenManager = new ScreenManager(container);
			_networkHelper = new NetworkStatusURLHelper();
			_serverManager = new ServerManager();
			_storeManager = new Store(network.isConnected);
			_videoHelper = new VideoConvertionHelper(container);
			
			// load family config from the local shared object
			_dataManager.loadFamily();
			
			showInitScreen();
		}
		
		private function showInitScreen():void
		{
			screen.showScreen(ScreenName.INTRO_SCREEN);
			setTimeout(showStartScreen, 1500);
		}
		
		private function showStartScreen():void
		{
			//screen.showPopup(PopupName.VIDEO_READY_POPUP);
			//screen.showScreen(ScreenName.HELLO_WORLD_SCREEN);
			if (DEBUG_MODE)
			{
				screen.showScreen(ScreenName.VIDEO_MENU_SCREEN);
			}
			else 
			{
				screen.showScreen(ScreenName.START_SCREEN);
			}
		}
		
		// get/set
		public function get container():Sprite { return _container; }
		public function get stageManager():StageManager { return _stageManager; }
		public function get sizeManager():AppSizeManager { return _sizeManager; }
		public function get data():DataManager { return _dataManager; }
		public function get screen():ScreenManager { return _screenManager; }
		public function get server():ServerManager { return _serverManager; }
		public function get store():Store { return _storeManager; }
		public function get network():INetworkStatusHelper { return _networkHelper; }
		public function get videoHelper():VideoConvertionHelper { return _videoHelper; }
	}
}