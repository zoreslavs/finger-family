package com.data
{
	import com.popup.CameraPermissionPopup;
	import com.popup.ChoosePhotoPopup;
	import com.popup.ConnectionPopup;
	import com.popup.EmptyFieldsPopup;
	import com.popup.FacebookInstalledPopup;
	import com.popup.GalleryPermissionPopup;
	import com.popup.InstagramInstalledPopup;
	import com.popup.InvalidEmailPopup;
	import com.popup.LoadingPopup;
	import com.popup.MicPermissionPopup;
	import com.popup.NoVideosPopup;
	import com.popup.PurchaseFailedPopup;
	import com.popup.Samsung7VideoPopup;
	import com.popup.ServerErrorPopup;
	import com.popup.SessionsPopup;
	import com.popup.UserExistsPopup;
	import com.popup.UserNotExistsPopup;
	import com.popup.VideoReadyPopup;
	import com.popup.VideoSavedPopup;
	
	public class PopupName
	{
		public static const LOADING_POPUP:Class = LoadingPopup;
		public static const NO_VIDEOS_POPUP:Class = NoVideosPopup;
		public static const CONNECTION_POPUP:Class = ConnectionPopup;
		public static const PURCHASE_FAILED_POPUP:Class = PurchaseFailedPopup;
		public static const SESSIONS_POPUP:Class = SessionsPopup;
		public static const VIDEO_READY_POPUP:Class = VideoReadyPopup;
		public static const VIDEO_SAVED_POPUP:Class = VideoSavedPopup;
		public static const CHOOSE_PHOTO_POPUP:Class = ChoosePhotoPopup;
		public static const USER_NOT_EXISTS_POPUP:Class = UserNotExistsPopup;
		public static const USER_EXISTS_POPUP:Class = UserExistsPopup;
		public static const SERVER_ERROR_POPUP:Class = ServerErrorPopup;
		public static const INVALID_EMAIL_POPUP:Class = InvalidEmailPopup;
		public static const EMPTY_FIELDS_POPUP:Class = EmptyFieldsPopup;
		public static const SAMSUNG7_VIDEO_POPUP:Class = Samsung7VideoPopup;
		public static const CAMERA_PERMISSION_POPUP:Class = CameraPermissionPopup;
		public static const GALLERY_PERMISSION_POPUP:Class = GalleryPermissionPopup;
		public static const MIC_PERMISSION_POPUP:Class = MicPermissionPopup;
		public static const FB_INSTALLED_POPUP:Class = FacebookInstalledPopup;
		public static const INSTAGRAM_INSTALLED_POPUP:Class = InstagramInstalledPopup;
	}
}