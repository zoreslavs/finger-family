package com.data
{
	public class SoundData
	{
		[Embed(source="../../../sound/music_empty.mp3")]
		public static var MUSIC_EMPTY:Class;
		
		[Embed(source="../../../sound/music_start.mp3")]
		public static var MUSIC_START:Class;
		
		[Embed(source="../../../sound/music_end.mp3")]
		public static var MUSIC_END:Class;
		
		[Embed(source="../../../sound/music_here_i_am.mp3")]
		public static var MUSIC_HERE_I_AM:Class;
		
		[Embed(source="../../../sound/music_how_do_you_do.mp3")]
		public static var MUSIC_HOW_DO_YOU_DO:Class;
		
		[Embed(source="../../../sound/music_how_do_you_do_joined.mp3")]
		public static var MUSIC_HOW_DO_YOU_DO_JOINED:Class;
		
		[Embed(source="../../../sound/how_do_you_do.mp3")]
		public static var VOICE_DEFAULT:Class;
		
		[Embed(source="../../../sound/fingers/daddy_finger.mp3")]
		public static var DADDY:Class;
		
		[Embed(source="../../../sound/fingers/mommy_finger.mp3")]
		public static var MOMMY:Class;
		
		[Embed(source="../../../sound/fingers/baby_finger.mp3")]
		public static var BABBY:Class;
		
		[Embed(source="../../../sound/fingers/son_finger.mp3")]
		public static var SON:Class;
		
		[Embed(source="../../../sound/fingers/daughter_finger.mp3")]
		public static var DAUGHTER:Class;
		
		[Embed(source="../../../sound/fingers/brother_finger.mp3")]
		public static var BROTHER:Class;
		
		[Embed(source="../../../sound/fingers/sister_finger.mp3")]
		public static var SISTER:Class;
		
		[Embed(source="../../../sound/fingers/grandpa_finger.mp3")]
		public static var GRANDPA:Class;
		
		[Embed(source="../../../sound/fingers/grandma_finger.mp3")]
		public static var GRANDMA:Class;
		
		[Embed(source="../../../sound/fingers/uncle_finger.mp3")]
		public static var UNCLE:Class;
		
		[Embed(source="../../../sound/fingers/aunt_finger.mp3")]
		public static var AUNT:Class;
		
		[Embed(source="../../../sound/fingers/cousin_finger.mp3")]
		public static var COUSIN_MALE:Class;
		
		[Embed(source="../../../sound/fingers/cousin_finger.mp3")]
		public static var COUSIN_FEMALE:Class;
		
		[Embed(source="../../../sound/fingers/friend_finger.mp3")]
		public static var FRIEND:Class;
		
		[Embed(source="../../../sound/fingers/teacher_finger.mp3")]
		public static var TEACHER:Class;
		
		[Embed(source="../../../sound/fingers/doggie_finger.mp3")]
		public static var DOGGIE:Class;
		
		[Embed(source="../../../sound/fingers/kitty_finger.mp3")]
		public static var KITTY:Class;
		
		public static const SOUND_ARRAY:Array = [DADDY, MOMMY, BABBY, SON, DAUGHTER, BROTHER, SISTER, GRANDPA, GRANDMA, UNCLE, AUNT, COUSIN_MALE, COUSIN_FEMALE, FRIEND, TEACHER, DOGGIE, KITTY];
	}
}