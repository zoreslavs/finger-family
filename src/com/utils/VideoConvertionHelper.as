package com.utils
{
	import com.Application;
	import com.abstract.ISoundHelper;
	import com.rainbowcreatures.FWVideoEncoder;
	
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.StatusEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	
	public class VideoConvertionHelper
	{
		private static const VIDEO_SCALE:Number = 0.5;
		
		private var _container:Sprite;
		private var _fwEncoder:FWVideoEncoder;
		private var _soundMixer:ISoundHelper;
		private var _captureArea:MovieClip;
		private var _frameIndex:int = 0;
		private var _totalFrames:int = 50;
		private var _onAudioReadyCallback:Function;
		private var _onStartCallback:Function;
		private var _onFinishCallback:Function;
		private var _onEncodeCallback:Function;
		private var _onFailCallback:Function;
		private var _onSaveCallback:Function;
		private var _soundBytes:ByteArray;
		private var _sound:Sound;
		private var _channel:SoundChannel;
		private var _isInitialized:Boolean;
		
		private var _bitmapData:BitmapData;
		private var _bounds:Rectangle;
		private var _matrix:Matrix;
		private var _bitmaps:Array = [];
		
		private var _frameTime:Number = 0;
		private var _lastFrameTime:Number = 0;
		
		public function VideoConvertionHelper(container:Sprite)
		{
			_container = container;
			
			_fwEncoder = FWVideoEncoder.getInstance(_container);
			_soundMixer = (VideoEnablingHelper.isVideoRealtimeEnabled) ? new SoundMixHelper() : new SoundPlayHelper();
			
			if (!VideoEnablingHelper.isVideoRealtimeEnabled)
			{
				_frameTime = (1 / _container.stage.frameRate) * 1000;
			}
		}
		
		public function init(captureArea:MovieClip, totalFrames:int, soundBytes:ByteArray, sound:Sound, onAudioReadyCallback:Function, onHandAnimCallback:Function, onZoomAnimCallback:Function, onRepeatAnimCallback:Function, onStartCallback:Function, onFinishCallback:Function, onEncodeCallback:Function, onFailCallback:Function):void
		{
			_captureArea = captureArea;
			_frameIndex = 0;
			_totalFrames = totalFrames;
			_onAudioReadyCallback = onAudioReadyCallback;
			_onStartCallback = onStartCallback;
			_onFinishCallback = onFinishCallback;
			_onEncodeCallback = onEncodeCallback;
			_onFailCallback = onFailCallback;
			
			if (!VideoEnablingHelper.isVideoEnabled)
			{
				_soundMixer.init(_fwEncoder, onHandAnimCallback, onZoomAnimCallback, onRepeatAnimCallback, onAudioFinish);
				_soundMixer.initSoundQueue(startAnimation);
				
				function startAnimation():void
				{
					_onStartCallback.call();
					_soundMixer.start();
				}
				return;
			}
			
			if (!VideoEnablingHelper.isVideoRealtimeEnabled)
			{
				_bitmapData = new BitmapData(_container.stage.fullScreenHeight * VIDEO_SCALE, _container.stage.fullScreenWidth * VIDEO_SCALE, false, 0x000000);
				_bounds = new Rectangle(0, 0, _bitmapData.width, _bitmapData.height);
				_matrix = new Matrix();
				_matrix.scale(VIDEO_SCALE, VIDEO_SCALE);
				
				_channel = new SoundChannel();
			}
			
			if (_isInitialized)
			{
				_soundMixer.init(_fwEncoder, onHandAnimCallback, onZoomAnimCallback, onRepeatAnimCallback, onAudioFinish);
				_soundMixer.initSoundQueue(_onAudioReadyCallback);
			}
			else 
			{
				_isInitialized = true;
				
				if (!VideoEnablingHelper.isVideoRealtimeEnabled)
				{
					if (PlatformCheckHelper.isIOS) {
						_fwEncoder.setDimensions(_captureArea.width, _captureArea.height);
					} else {
						_fwEncoder.setDimensions(_bitmapData.width, _bitmapData.height);
					}
				}
				
				_soundBytes = soundBytes;
				_sound = sound;
				_fwEncoder.addEventListener(StatusEvent.STATUS, onStatus);
			
				_soundMixer.init(_fwEncoder, onHandAnimCallback, onZoomAnimCallback, onRepeatAnimCallback, onAudioFinish);
				_soundMixer.initSoundQueue(_onAudioReadyCallback);
				
				Application.instance.stageManager.addEventListener(Event.ACTIVATE, onStageActivationChanged);
				Application.instance.stageManager.addEventListener(Event.DEACTIVATE, onStageActivationChanged);
			}
			
			if (!VideoEnablingHelper.isVideoRealtimeEnabled)
				_onAudioReadyCallback.call();
		}
		
		private function onStageActivationChanged(e:Event):void
		{
			if (!Application.instance.stageManager.isActivated)
			{
				if (_container.hasEventListener(Event.ENTER_FRAME))
					_container.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				
				_soundMixer.stop();
				_fwEncoder.stop();
				
				//_onFailCallback.call();
			}
		}
		
		public function recordVideo():void
		{
			if (_fwEncoder.isRecording)
				return;
			
			_fwEncoder.load();
		}
		
		public function getRecordedVideo():ByteArray
		{
			var bytes:ByteArray = _fwEncoder.getVideo();
			/*var fileName:String = "video/video.mp4";
			var file:File = File.applicationStorageDirectory.resolvePath(fileName);
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.READ);
			stream.readBytes(bytes, 0, stream.bytesAvailable);
			stream.close();*/
			
			return bytes;
		}
		
		public function saveVideoToGallery(onSaveCallback:Function):void
		{
			_onSaveCallback = onSaveCallback;
			
			if (_fwEncoder.platform == FWVideoEncoder.PLATFORM_IOS || _fwEncoder.platform == FWVideoEncoder.PLATFORM_ANDROID)
			{
				_fwEncoder.saveToGallery();
			}
		}
		
		public function saveVideoToFile(onSaveCallback:Function):void
		{
			_onSaveCallback = onSaveCallback;
			
			var fileName:String = "videos/video.mp4";
			var file:File = File.applicationStorageDirectory.resolvePath(fileName);
			var bytes:ByteArray = _fwEncoder.getVideo();
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(bytes, 0, bytes.length);
			stream.close();
			
			_onSaveCallback(fileName);
		}
		
		private function onStatus(e:StatusEvent):void
		{
			trace("VideoConvertionHelper - onStatus: ", e.code);
			if (e.code == "ready") {
				// FlashyWrappers ready, capture in non-realtime mode(realtime set to false), with no audio
				//_fwEncoder.setCaptureRectangle(_captureArea.x, _captureArea.y, _captureArea.width, _captureArea.height);
				
				if (VideoEnablingHelper.isVideoRealtimeEnabled)
					_fwEncoder.start(20, FWVideoEncoder.AUDIO_STEREO);
				else 
					_fwEncoder.start(20, FWVideoEncoder.AUDIO_STEREO, false);
			}
			if (e.code == "started") {
				
				_onStartCallback.call();
				
				_container.addEventListener(Event.ENTER_FRAME, onEnterFrame);
				
				_soundMixer.start();
				
				/*_sound = new SoundData.MUSIC_EMPTY();
				_fwEncoder.addSoundtrack(_sound);
				
				_channel = _sound.play();
				_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);*/
				
				/*if (PlatformCheckHelper.isIOS)
				{
					if (_sound)
						_fwEncoder.addSoundtrack(_sound);
					
					//if (_soundBytes)
						//_fwEncoder.addAudioFrame(_soundBytes);
				}
				else 
				{
					//if (_sound)
						//_fwEncoder.addSoundtrack(_sound);
					
					if (_soundBytes)
						_fwEncoder.addAudioFrameShorts(_soundBytes);
				}*/
			}
			if (e.code == "encoded") {
				_onEncodeCallback.call();
				
				if (_container.hasEventListener(Event.ENTER_FRAME))
					_container.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			}
			if (e.code == "gallery_saved") {
				if (_onSaveCallback)
					_onSaveCallback.call();
			}
		}
		
		private function onEnterFrame(e:Event):void {
			if (_frameIndex >= _totalFrames - 1)
			{
				if (_container.hasEventListener(Event.ENTER_FRAME))
					_container.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				
				_soundMixer.stop();
				
				/*for each (var bitmapData:BitmapData in _bitmaps)
				{
					_fwEncoder.addVideoFrame(bitmapData.getPixels(_bounds));
				}*/
				
				if (_fwEncoder.isRecording)
					_fwEncoder.finish();
				
				_onFinishCallback.call();
			}
			else 
			{
				// capture the whole stage every frame - you can also capture individual DisplayOjects
				if (VideoEnablingHelper.isVideoRealtimeEnabled)
				{
					_fwEncoder.capture();
				}
				else 
				{
					//_bitmapData = new BitmapData(_container.stage.fullScreenWidth, _container.stage.fullScreenHeight);
					_bitmapData.draw(_container.stage, _matrix);
					var frameBytes:ByteArray = _bitmapData.getPixels(_bounds);
					_fwEncoder.addVideoFrame(frameBytes);
					
					//trace("getTimer(): ", Math.floor((getTimer() - _lastFrameTime) / _frameTime));
					if (getTimer() - _lastFrameTime > _frameTime)
					{
						var numFrames:int = Math.floor((getTimer() - _lastFrameTime) / _frameTime);
						for (var i:int = 0; i < numFrames; i++) 
						{
							_fwEncoder.addVideoFrame(frameBytes);
						}
					}
					
					_lastFrameTime = getTimer();
					
					//_bitmaps.push(_bitmapData);
					
					/*var curFPS:int = (getFPS() > 0) ? getFPS() : lastFPS;
					if (curFPS > 0)
					{
					var numFrames:int = Math.floor(_container.stage.frameRate / curFPS);
					for (var i:int = 0; i < numFrames; i++) 
					{
					_fwEncoder.addVideoFrame(_bitmapData.getPixels(_bounds));
					}
					}*/
				}
				_frameIndex++;
				
				/*if (!VideoEnablingHelper.isVideoRealtimeEnabled && _frameIndex == 120)
				{
					onAudioFinish();
				}*/
			}
		}
		
		private function onSoundComplete(e:Event):void
		{
			onAudioFinish();
		}
		
		private function onAudioFinish():void
		{
			if (_container.hasEventListener(Event.ENTER_FRAME))
				_container.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			_soundMixer.stop();
			
			if (_fwEncoder.isRecording)
				_fwEncoder.finish();
			
			_onFinishCallback.call();
		}
		
		private var framesNumber:int;
		private var lastFPS:int;
		private var startTime:Number = getTimer();
		private function getFPS():int
		{
			var result:int = 0;
			var currentTime:Number = (getTimer() - startTime) / 1000;
			
			framesNumber++;
			
			if (currentTime > 1)
			{
				result = (Math.floor((framesNumber/currentTime)*10.0)/10.0);
				lastFPS = result;
				startTime = getTimer();
				framesNumber = 0;
			}
			
			return result;
		}
	}
}