package com.utils
{
	import com.Application;
	import com.abstract.INetworkStatusHelper;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.StatusEvent;
	import flash.net.URLRequest;
	
	import air.net.URLMonitor;

	public class NetworkStatusURLHelper implements INetworkStatusHelper
	{
		private var _monitor:URLMonitor;
		private var _isConnected:Boolean = true;
		
		public function NetworkStatusURLHelper()
		{
			init();
		}
		
		private function init():void
		{
			checkNetConnection(null);
			NativeApplication.nativeApplication.addEventListener(Event.NETWORK_CHANGE, checkNetConnection);
		}
		
		private function checkNetConnection(e:Event):void
		{
			_monitor = new URLMonitor(new URLRequest("http://www.google.com"));
			if (!_monitor.hasEventListener(StatusEvent.STATUS))
			{
				_monitor.addEventListener(StatusEvent.STATUS, onStatusChange);
			}
			_monitor.start();
		}
		
		private function onStatusChange(e:StatusEvent):void
		{
			_isConnected = _monitor.available;
			trace("onStatusChange: " + _isConnected);
			if (_isConnected)
				Application.instance.store.init();
		}
		
		// get/set
		public function get isConnected():Boolean { return _isConnected; }
	}
}