package com.utils
{
	import flash.display.Sprite;
	import flash.system.Capabilities;

	public class PlatformCheckHelper
	{
		private static var _isSimulator:Boolean;
		private static var _isIOS:Boolean;
		private static var _isAndroid:Boolean;
		private static var _isAmazon:Boolean;
		private static var _isIPad:Boolean;
		private static var _isPhone:Boolean;
		
		public static function checkPlatform(container:Sprite):void
		{
			if (Capabilities.os.indexOf("iPhone") != -1)
			{
				_isIOS = true;
				_isIPad = (Capabilities.os.indexOf("iPad") != -1);
				_isPhone = !_isIPad;
			}
			else if (Capabilities.os.indexOf("ARM") != -1 || Capabilities.os.indexOf("Linux") != -1)
			{
				_isAndroid = true;
				_isPhone = isPhoneDevice(container);
			}
			else 
			{
				_isSimulator = true;
			}
		}
		
		public static function isPhoneDevice(container:Sprite):Boolean
		{
			var tabletMinimumInches:Number = 5;
			return(Math.max(container.stage.fullScreenWidth, container.stage.fullScreenHeight) / Capabilities.screenDPI < tabletMinimumInches);
		}
		
		// get/set
		public static function get isSimulator():Boolean { return _isSimulator; }
		public static function get isIOS():Boolean { return _isIOS; }
		public static function get isAndroid():Boolean { return _isAndroid; }
		public static function get isAmazon():Boolean { return _isAmazon; }
		public static function get isIPad():Boolean { return _isIPad; }
		public static function get isPhone():Boolean { return _isPhone; }
	}
}