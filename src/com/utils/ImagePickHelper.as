package com.utils
{
	import com.freshplanet.ane.AirImagePicker.AirImagePicker;
	
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MediaEvent;
	import flash.geom.Matrix;
	import flash.media.CameraRoll;
	import flash.media.CameraUI;
	import flash.media.MediaPromise;
	import flash.media.MediaType;
	import flash.utils.ByteArray;

	public class ImagePickHelper
	{
		private static var _successCallback:Function;
		private static var _cancelCallback:Function;
		private static var _isCameraImage:Boolean;

		public function ImagePickHelper():void
		{
			AirImagePicker.getInstance().logEnabled = false;
		}
		
		public static function pickImage(successCallback:Function, cancelCallback:Function, cropping:Boolean = false):void
		{
			_successCallback = successCallback;
			_cancelCallback = cancelCallback;
			
			_isCameraImage = false;
			if (AirImagePicker.getInstance().isImagePickerAvailable())
			{
				AirImagePicker.getInstance().displayImagePicker(onImagePicked);
			}
			else 
			{
				var cameraRoll:CameraRoll = new CameraRoll();
				if (CameraRoll.supportsBrowseForImage)
				{
					cameraRoll.addEventListener(MediaEvent.SELECT, mediaEventComplete);
					cameraRoll.addEventListener(Event.CANCEL, browseCanceled);
					cameraRoll.addEventListener(ErrorEvent.ERROR, mediaError);
					cameraRoll.browseForImage();
				}
				else
				{
					trace("Image browsing is not supported on this device.");
					_cancelCallback.call();
				}
			}
		}

		public static function takePhoto(successCallback:Function, cancelCallback:Function, cropping:Boolean = false):void
		{
			_successCallback = successCallback;
			_cancelCallback = cancelCallback;
			
			_isCameraImage = true;
			if (AirImagePicker.getInstance().isCameraAvailable())
			{
				AirImagePicker.getInstance().displayCamera(onImagePicked);
			}
			else 
			{
				if (CameraUI.isSupported)
				{
					var camera:CameraUI = new CameraUI();
					camera.addEventListener(MediaEvent.COMPLETE, mediaEventComplete);
					camera.addEventListener(Event.CANCEL, browseCanceled);
					camera.addEventListener(ErrorEvent.ERROR, mediaError);
					camera.launch(MediaType.IMAGE);
				}
				else
				{
					trace("Camera is not available on this device");
					_cancelCallback.call();
				}
			}
		}

		private static function onImagePicked(status:String, image:BitmapData = null, data:ByteArray = null):void
		{
			trace("[AirImagePicker Test] ANE returned status - ", status);
			
			if (image)
				trace("[AirImagePicker Test] ANE returned image of " + image.width + "x" + image.height);
			
			if (data)
			{
				trace("[AirImagePicker Test] ANE returned JPEG data of " + data.length + " bytes");
				loadImage(data);
			}
			else 
			{
				_cancelCallback.call();
			}
		}
		
		private static function mediaError(event:ErrorEvent):void
		{
			trace("Media error");
			_cancelCallback.call();
		}
		
		private static function browseCanceled(event:Event):void
		{
			trace("Browse canceled");
			_cancelCallback.call();
		}
		
		protected static function mediaEventComplete(event:MediaEvent):void
		{
			var mediaPromise:MediaPromise = event.data;
			var mpLoader:Loader = new Loader();
			mpLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onMediaLoaded);
			mpLoader.loadFilePromise(mediaPromise);
		}
		
		private static function onMediaLoaded(event:Event):void
		{
			var bitmapData:BitmapData = event.target.content.bitmapData;
			if (PlatformCheckHelper.isAndroid && _isCameraImage)
			{
				/*var bounds:Rectangle = new Rectangle(0, 0, bitmapData.width, bitmapData.height);
				var pixels:ByteArray = bitmapData.getPixels(bounds);
				pixels.position = 0;
				trace("Orientation: ", getOrientation(pixels));*/
				
				// rotate bitmap data
				bitmapData = rotateBitmapData(bitmapData, 90);
			}
			_successCallback.call(null, bitmapData);
		}
		
		private static function loadImage(data:ByteArray):void
		{
			var loader:Loader = new Loader();
			loader.loadBytes(data);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loaderComplete);
			
			function loaderComplete(event:Event):void
			{
				var loaderInfo:LoaderInfo = LoaderInfo(event.target);
				var bitmapData:BitmapData = new BitmapData(loaderInfo.width, loaderInfo.height, false, 0xFFFFFF);
				bitmapData.draw(loaderInfo.loader);
				
				_successCallback.call(null, bitmapData);
			}
		}
		
		private static function rotateBitmapData(bitmapData:BitmapData, degree:int = 0):BitmapData
		{
			var newBitmap:BitmapData = new BitmapData(bitmapData.height, bitmapData.width, true);
			var matrix:Matrix = new Matrix();
			matrix.rotate(degree * Math.PI / 180);
			
			if (degree == 90) {
				matrix.translate(bitmapData.height, 0);
			} else if (degree == -90 || degree == 270) {
				matrix.translate(0, bitmapData.width);
			} else if (degree == 180) {
				newBitmap = new BitmapData(bitmapData.width, bitmapData.height, true);
				matrix.translate(bitmapData.width, bitmapData.height);
			}
			
			newBitmap.draw(bitmapData, matrix, null, null, null, true);
			return newBitmap;
		}
		
		/**1: normal<br>
		 3 rotated 180 degrees (upside down)<br>
		 6: rotated 90 degrees CW<br>
		 8: rotated 90 degrees CCW<br>
		 9: unknown<br>
		 */
		/*private static function getOrientation(ImageBytes:ByteArray):uint
		{
			var exif:ExifInfo = new ExifInfo(ImageBytes);
			if(exif.ifds != null)
			{
				var ifd:IFD = exif.ifds.primary;
				var str:String = "";
				for (var entry:String in ifd) {
					if(entry == "Orientation"){
						str = ifd[entry];
						break;
					}
				}
				return uint(str);
			}
			else
			{
				return 9 ;
			}
		}*/
	}
}