package com.utils
{
	import com.Application;
	import com.abstract.ISoundHelper;
	import com.data.SoundData;
	import com.rainbowcreatures.FWSound;
	import com.rainbowcreatures.FWSoundMixer;
	import com.rainbowcreatures.FWVideoEncoder;
	
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;

	public class SoundMixHelper implements ISoundHelper
	{
		private static const TOTAL_SOUNDS_COUNT:int = 12;
		
		private var _mySoundMixer:FWSoundMixer;
		private var _mySoundChannel:SoundChannel = new SoundChannel();
		
		private var _musicSound:Sound;
		private var _musicFWSound:FWSound;
		private var _musicHDYDJoinedFWSound:FWSound;
		private var _sound:Sound;
		private var _channel:SoundChannel;
		
		private var _curFWSoundIndex:int;
		private var _curFWSound:FWSound;
		private var _fwSoundQueue:Array = [];
		private var _recIndexes:Array = [];
		private var _queueCompleteCallback:Function;
		private var _onHandAnimCallback:Function;
		private var _onZoomAnimCallback:Function;
		private var _onRepeatAnimCallback:Function;
		private var _onAudioFinishCallback:Function;
		private var _isActive:Boolean;
		private var _isAnimRepeated:Boolean;
		
		public function SoundMixHelper()
		{
			_mySoundMixer = FWSoundMixer.getInstance();
		}
		
		public function init(fwEncoder:FWVideoEncoder, onHandAnimCallback:Function, onZoomAnimCallback:Function, onRepeatAnimCallback:Function, onAudioFinishCallback:Function):void
		{
			_isActive = true;
			_onHandAnimCallback = onHandAnimCallback;
			_onZoomAnimCallback = onZoomAnimCallback;
			_onRepeatAnimCallback = onRepeatAnimCallback;
			_onAudioFinishCallback = onAudioFinishCallback;
			
			_mySoundMixer.init(sendData, fwEncoder);
			
			_musicHDYDJoinedFWSound = new FWSound(null, null, new SoundData.MUSIC_HOW_DO_YOU_DO_JOINED(), _mySoundMixer, true);
			
			//_musicSound = new SoundData.MUSIC_EMPTY();
			//_musicFWSound = new FWSound(null, null, _musicSound, _mySoundMixer, true);
		}
		
		public function start():void
		{
			//_mySoundChannel = _musicFWSound.play();
			
			_curFWSoundIndex = 0;
			_isAnimRepeated = false;
			
			playFWSound();
			
			if (VideoEnablingHelper.isVideoEnabled)
				_mySoundMixer.startCapture(false);
		}
		
		public function stop():void
		{
			_isActive = false;
			
			if (VideoEnablingHelper.isVideoEnabled)
				_mySoundMixer.stopCapture();
			//_mySoundMixer.stopAll();
			//_mySoundMixer.free();
		}
		
		public function initSoundQueue(completeCallback:Function):void
		{
			_queueCompleteCallback = completeCallback;
			
			_fwSoundQueue = [];
			_recIndexes = [];
			
			var soundIndex:int;
			var soundClass:Class;
			
			addSoundToQueue(SoundData.MUSIC_START);
			for (var i:int = 1; i <= 5; i++)
			{
				soundIndex = Application.instance.data.getFamilyMember(i) - 1;
				soundClass = SoundData.SOUND_ARRAY[soundIndex];
				addSoundToQueue(soundClass);
				
				if (Application.instance.data.getFamilyMemberVoice(i))
				{
					addSoundToQueue(null, Application.instance.data.getFamilyMemberVoice(i) as ByteArray);
				}
				else 
				{
					addSoundToQueue(SoundData.VOICE_DEFAULT);
				}
			}
			addSoundToQueue(SoundData.MUSIC_END);
		}
		
		private function playFWSound():void
		{
			if (!_isActive)
				return;
			
			_curFWSound = _fwSoundQueue[_curFWSoundIndex];
			_mySoundChannel = _curFWSound.play();
			_mySoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			
			// plays only with mic recording
			if (_recIndexes.indexOf(_curFWSoundIndex) != -1)
				_musicHDYDJoinedFWSound.play();
			
			if (_curFWSoundIndex % 2 != 0)
				_onHandAnimCallback.call();
			else if (_curFWSoundIndex > 0)
				_onZoomAnimCallback.call();
		}
		
		private function onSoundComplete(e:Event):void
		{
			_mySoundChannel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			_mySoundChannel.stop();
			
			// repeat animation
			if (!_isAnimRepeated && _curFWSoundIndex == 10)
			{
				_curFWSoundIndex = 0;
				_isAnimRepeated = true;
				playFWSound();
				_onRepeatAnimCallback.call();
				return;
			}
			
			//if (_curFWSoundIndex < 1)
			if (_curFWSoundIndex < _fwSoundQueue.length - 1)
			{
				_curFWSoundIndex++;
				playFWSound();
			}
			else 
			{
				_onAudioFinishCallback.call();
			}
		}
		
		private function sendData(e:Event = null):void
		{
		}
		
		private function addSoundToQueue(soundClass:Class, micRecBytes:ByteArray = null):void
		{
			var sound:Sound;
			if (soundClass)
			{
				sound = new soundClass();
			}
			else if (micRecBytes)
			{
				micRecBytes.position = 0;
				sound = new Sound();
				sound.loadPCMFromByteArray(micRecBytes, micRecBytes.bytesAvailable / 4, "float", false);
				
				_recIndexes.push(_fwSoundQueue.length);
			}
			var fwSound:FWSound = new FWSound(null, null, sound, _mySoundMixer, true);
			_fwSoundQueue.push(fwSound);
			
			if (_fwSoundQueue.length == TOTAL_SOUNDS_COUNT)
				_queueCompleteCallback.call();
		}
	}
}