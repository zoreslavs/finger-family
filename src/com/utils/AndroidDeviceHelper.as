package com.utils
{
	import nl.funkymonkey.android.deviceinfo.NativeDeviceInfo;
	import nl.funkymonkey.android.deviceinfo.NativeDeviceInfoEvent;
	import nl.funkymonkey.android.deviceinfo.NativeDeviceProperties;

	public class AndroidDeviceHelper
	{
		private static const MY_SAMSUNG:String = "SM-G800H";
		private static const SAMSUNG_7:String = "SM-G930";
		private static const SAMSUNG_7_VERIZON:String = "SM-G930V";
		private static const MOTOROLA_XT:String = "XT1254";
		
		public static function checkDevice():void
		{
			var deviceInfo:NativeDeviceInfo = new NativeDeviceInfo();
			deviceInfo.addEventListener(NativeDeviceInfoEvent.PROPERTIES_PARSED, onDevicePropertiesParse);
			deviceInfo.setDebug(false);
			deviceInfo.parse();
			
			function onDevicePropertiesParse(e:NativeDeviceInfoEvent):void
			{
				NativeDeviceInfo(e.target).removeEventListener(NativeDeviceInfoEvent.PROPERTIES_PARSED, onDevicePropertiesParse);
			}
		}
		
		public static function get isVideoEnabled():Boolean
		{
			var deviceInfo:String = NativeDeviceProperties.PRODUCT_MODEL.value;
			if (deviceInfo == SAMSUNG_7 || deviceInfo == SAMSUNG_7_VERIZON/* || deviceInfo == MY_SAMSUNG*/)
				return false;
			
			return true;
		}
		
		public static function get isVideoRealtimeEnabled():Boolean
		{
			var deviceInfo:String = NativeDeviceProperties.PRODUCT_MODEL.value;
			if (deviceInfo == SAMSUNG_7 || deviceInfo == SAMSUNG_7_VERIZON/* || deviceInfo == MY_SAMSUNG*/)
				return false;
			
			return true;
		}
	}
}