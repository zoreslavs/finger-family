package com.utils
{
	import com.Application;
	import com.abstract.ISoundHelper;
	import com.data.SoundData;
	import com.rainbowcreatures.FWVideoEncoder;
	
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	
	public class SoundPlayHelper implements ISoundHelper
	{
		private static const TOTAL_SOUNDS_COUNT:int = 12;
		
		private var _mySoundChannel:SoundChannel = new SoundChannel();
		private var _musicHDYDJoinedSound:Sound;
		
		private var _fwEncoder:FWVideoEncoder;
		private var _curSoundIndex:int;
		private var _curSound:Sound;
		private var _soundQueue:Array = [];
		private var _recIndexes:Array = [];
		private var _queueCompleteCallback:Function;
		private var _onHandAnimCallback:Function;
		private var _onZoomAnimCallback:Function;
		private var _onRepeatAnimCallback:Function;
		private var _onAudioFinishCallback:Function;
		private var _isActive:Boolean;
		private var _isAnimRepeated:Boolean;
		
		public function SoundPlayHelper()
		{
			_musicHDYDJoinedSound = new SoundData.MUSIC_HOW_DO_YOU_DO_JOINED();
		}
		
		public function init(fwEncoder:FWVideoEncoder, onHandAnimCallback:Function, onZoomAnimCallback:Function, onRepeatAnimCallback:Function, onAudioFinishCallback:Function):void
		{
			_isActive = true;
			_fwEncoder = fwEncoder;
			_onHandAnimCallback = onHandAnimCallback;
			_onZoomAnimCallback = onZoomAnimCallback;
			_onRepeatAnimCallback = onRepeatAnimCallback;
			_onAudioFinishCallback = onAudioFinishCallback;
		}
		
		public function start():void
		{
			_curSoundIndex = 0;
			_isAnimRepeated = false;
			playSound();
		}
		
		public function stop():void
		{
			_isActive = false;
		}
		
		public function initSoundQueue(completeCallback:Function):void
		{
			_queueCompleteCallback = completeCallback;
			
			_soundQueue = [];
			_recIndexes = [];
			
			var soundIndex:int;
			var soundClass:Class;
			
			addSoundToQueue(SoundData.MUSIC_START);
			for (var i:int = 1; i <= 5; i++)
			{
				soundIndex = Application.instance.data.getFamilyMember(i) - 1;
				soundClass = SoundData.SOUND_ARRAY[soundIndex];
				addSoundToQueue(soundClass);
				
				if (Application.instance.data.getFamilyMemberVoice(i))
				{
					addSoundToQueue(null, Application.instance.data.getFamilyMemberVoice(i) as ByteArray);
				}
				else 
				{
					addSoundToQueue(SoundData.VOICE_DEFAULT);
				}
			}
			addSoundToQueue(SoundData.MUSIC_END);
		}
		
		private function playSound():void
		{
			if (!_isActive)
				return;
			
			_curSound = _soundQueue[_curSoundIndex];
			_mySoundChannel = _curSound.play();
			_mySoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			if (_fwEncoder.isRecording)
				_fwEncoder.addSoundtrack(_curSound);
			
			// plays only with mic recording
			if (_recIndexes.indexOf(_curSoundIndex) != -1)
			{
				_musicHDYDJoinedSound.play();
				if (_fwEncoder.isRecording)
					_fwEncoder.addSoundtrack(_musicHDYDJoinedSound);
			}
			
			if (_curSoundIndex % 2 != 0)
				_onHandAnimCallback.call();
			else if (_curSoundIndex > 0)
				_onZoomAnimCallback.call();
		}
		
		private function onSoundComplete(e:Event):void
		{
			_mySoundChannel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			_mySoundChannel.stop();
			
			// repeat animation
			if (!_isAnimRepeated && _curSoundIndex == 10)
			{
				_curSoundIndex = 0;
				_isAnimRepeated = true;
				playSound();
				_onRepeatAnimCallback.call();
				return;
			}
			
			if (_curSoundIndex < _soundQueue.length - 1)
			{
				_curSoundIndex++;
				playSound();
			}
			else 
			{
				_onAudioFinishCallback.call();
			}
		}
		
		private function addSoundToQueue(soundClass:Class, micRecBytes:ByteArray = null):void
		{
			var sound:Sound;
			if (soundClass)
			{
				sound = new soundClass();
			}
			else if (micRecBytes)
			{
				micRecBytes.position = 0;
				sound = new Sound();
				sound.loadPCMFromByteArray(micRecBytes, micRecBytes.bytesAvailable / 4, "float", false);
				
				_recIndexes.push(_soundQueue.length);
			}
			_soundQueue.push(sound);
			
			if (_soundQueue.length == TOTAL_SOUNDS_COUNT)
				_queueCompleteCallback.call();
		}
	}
}