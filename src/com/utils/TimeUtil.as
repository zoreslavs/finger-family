package com.utils
{
    public class TimeUtil
    {
        public static var hoursTimeFormat:Object = {hrs:true, min:true, sec:true, ms:false};
		public static var minutesTimeFormat:Object = {hrs:false, min:true, sec:true, ms:false};
		
        //converts time to output string: input int(ms) - output format - hh:mm:ss:ms
        public static function timeParse(time:Number, isMinTimeFormat:Boolean = false):String
        {
			var timeFormat:Object = (isMinTimeFormat) ? minutesTimeFormat : hoursTimeFormat;
            var hr:int = Math.floor( time / 3600000 );
            var mn:int = Math.floor( (time % 3600000) / 60000 );
            var sc:int = Math.floor( ((time % 3600000) % 60000) / 1000 );
            var ms:int = Math.floor( (((time % 3600000) % 60000) % 1000) / 10 ); //reduce to hundreths

            var hrs:String = (hr > 0) ? ( (hr < 10) ? '0'+hr+':' : hr+':' ) : '00:';
            var min:String = (mn > 0 || hr > 0) ? ( (mn < 10) ? '0'+mn+':' : mn+':' ) : '00:'; //mn+':' : '0:';
            var sec:String = (sc < 10) ? '0'+sc+( (timeFormat.ms) ? ':' : '' ) : sc+( (timeFormat.ms) ? ':' : '' );
            var mls:String = (ms < 10) ? '0'+ms : String(ms);

            var format:String = "";
            if (timeFormat.hrs)
                format += hrs;
            if (timeFormat.min)
                format += min;
            if (timeFormat.sec)
                format += sec;
            if (timeFormat.ms)
                format += mls;

            return format;
        }
    }
}