package com.utils
{
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.AudioPlaybackMode;
	import flash.media.Microphone;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.permissions.PermissionStatus;
	import flash.utils.ByteArray;

	public class AudioRecordHelper
	{
		private var _mic:Microphone;
		private var _soundData:ByteArray;
		private var _saveToFile:Boolean;
		private var _sound:Sound;
		private var _channel:SoundChannel;
		//private var _wavBytes:ByteArray;
		//private var _mp3Encoder:ShineMP3Encoder;
		private var _completeCallback:Function;

		public function AudioRecordHelper(completeCallback:Function)
		{
			_completeCallback = completeCallback;
			
			SoundMixer.useSpeakerphoneForVoice = true;
			SoundMixer.audioPlaybackMode = AudioPlaybackMode.MEDIA;
		}
		
		public function startRecording():void
		{
			if (Microphone.isSupported)
			{
				_soundData = new ByteArray();
				
				//Microphone.getEnhancedMicrophone();
				if (Microphone.getMicrophone() != null/* && Microphone.permissionStatus == PermissionStatus.GRANTED*/)
				{
					_mic = Microphone.getMicrophone();
					_mic.rate = 44;
					_mic.gain = 80;
					_mic.setSilenceLevel(5, 1000);
					
					_mic.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataWrite);
				}
			}
			else
			{
				trace("No microphone support!");
			}
		}
		
		public function stopRecording():void
		{
			if (_mic != null)
			{
				_mic.setLoopBack(false);
				_mic.removeEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataWrite);
			}
			
			//playRecording(true);
		}
		
		public function playRecording(mute:Boolean = false):void
		{
			/*_sound = new Sound();
			_sound.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataRead);
			var volume:int = (mute) ? 0 : 1;
			var trans:SoundTransform = new SoundTransform(volume);
			_channel = _sound.play(0, 0, trans);
			_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);*/
			
			if (!_soundData || !_soundData.length)
				return;
			
			_soundData.position = 0;
			_sound = new Sound();
			_sound.loadPCMFromByteArray(_soundData, _soundData.bytesAvailable / 4, "float", false);
			var volume:int = (mute) ? 0 : 1;
			var trans:SoundTransform = new SoundTransform(volume);
			_channel = _sound.play(0, 0, trans);
		}
		
		public function getRecordingBytes():ByteArray
		{
			return _soundData;
		}
		
		public function getRecordingSound():Sound
		{
			return _sound;
		}
		
		private function onSampleDataWrite(e:SampleDataEvent):void
		{
			while (e.data.bytesAvailable)
			{
				var sample:Number = e.data.readFloat();
				_soundData.writeFloat(sample);
			}
		}
		
		private function onSampleDataRead(e:SampleDataEvent):void
		{
			if (!_soundData.bytesAvailable > 0)
				return;
			
			for (var i:int = 0; i < 8192; i++)
			{
				var sample:Number = 0;
				if (_soundData.bytesAvailable > 0)
					sample = _soundData.readFloat();
				
				e.data.writeFloat(sample);
				e.data.writeFloat(sample);
			}
		}
		
		private function onSoundComplete(e:Event):void
		{
			_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			_sound.removeEventListener(SampleDataEvent.SAMPLE_DATA, onSampleDataRead);
			
			_completeCallback.call();
		}
		
		private function saveRecording(bytes:ByteArray):void
		{
			var fileName:String = "recordings/recording.wav";
			var file:File = File.documentsDirectory.resolvePath(fileName);
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(bytes, 0, bytes.length);
			stream.close();
		}
		
		/*private function encodeToWAV():void
		{
			_wavBytes = new ByteArray();
			var writer:WAVWriter = new WAVWriter();
			writer.numOfChannels = 2;
			writer.sampleBitRate = 16;
			writer.samplingRate = 44100;
			writer.processSamples(_wavBytes, _soundData, 44100, 2);
			_wavBytes.position = 0;
		}*/
		
		/*private function encodeToMP3():void
		{
			_mp3Encoder = new ShineMP3Encoder(wavBytes);
			_mp3Encoder.addEventListener(Event.COMPLETE, onMP3EncoderComplete);
			_mp3Encoder.addEventListener(ProgressEvent.PROGRESS, onMP3EncoderProcess);
			_mp3Encoder.start();
			
			function onMP3EncoderProcess(e:ProgressEvent):void
			{
				trace("Encoding to mp3 ... " + Math.ceil(e.bytesLoaded * 100 / e.bytesTotal) + "%");
			}
			
			function onMP3EncoderComplete(e:Event):void
			{
				trace("Encoding to mp3 complete!");
			}
		}*/
	}
}