package com.utils
{
	import com.sticksports.nativeExtensions.canOpenUrl.CanOpenUrl;

	public class InstalledAppCheckHelper
	{
		public static function get isFacebookAppInstalled():Boolean
		{
			return CanOpenUrl.canOpen("fb://page/6340028662");
		}
		
		public static function get isInstagramAppInstalled():Boolean
		{
			return CanOpenUrl.canOpen("instagram://user?username=USERNAME");
		}
	}
}