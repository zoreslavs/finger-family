package com.utils
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Linear;
	
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import fl.motion.MatrixTransformer;
	
	public class ImageZoomHelper
	{
		// Declare a variable for the transform matrix of the image object.
		private static var mat:Matrix;
		private static var externalCenter:Point;
		private static var internalCenter:Point;
		private static var scaleFactor:Number = 0.5;//0.8;
		private static var minScale:Number = 0.25;
		private static var maxScale:Number = 2.0;
		
		private static var _image:Sprite;
		
		public function ImageZoomHelper()
		{
		}
		
		public static function zoomImage(image:Sprite, position:Point, time:int, zoomIn:Boolean, tween:Boolean = true):void
		{
			_image = image;
			
			/* 
			Get the point at the mouse in terms of image coordinates and (its parent) spImage coordinates. These points are aligned when the mouse click happens but after image is scaled, they will not be aligned anymore.
			*/
			externalCenter = position;
			internalCenter = position;
			
			// We scaled the image up (but bounded by maxScale) or down (bounded by minScale) depending on whether the shift key
			//   or the ctrl key is down while the mouse click happens.
			var imageScaleX:Number;
			var imageScaleY:Number;
			if (zoomIn)
			{
				imageScaleX = Math.min(1/scaleFactor * image.scaleX, maxScale);
				imageScaleY = Math.min(1/scaleFactor * image.scaleY, maxScale);
				if (tween)
				{
					TweenLite.to(image, time, {scaleX:imageScaleX, scaleY:imageScaleY, ease:Linear.easeIn, onUpdate:onTweenUpdate, onComplete:onTweenComplete});
				}
				else 
				{
					image.scaleX = imageScaleX;
					image.scaleY = imageScaleY;
				}
			}
			else 
			{
				imageScaleX = Math.max(scaleFactor * image.scaleX, minScale);
				imageScaleY = Math.max(scaleFactor * image.scaleY, minScale);
				if (tween)
				{
					TweenLite.to(image, time, {scaleX:imageScaleX, scaleY:imageScaleY, ease:Linear.easeOut, onUpdate:onTweenUpdate, onComplete:onTweenComplete});
				}
				else 
				{
					image.scaleX = imageScaleX;
					image.scaleY = imageScaleY;
				}
			}
			
			if (!tween)
				setMatrixTransform();
		}
		
		private static function setMatrixTransform():void
		{
			/* 
			The mat matrix is the transformation matrix for the scaled version of image; i.e., the version that no longer has internalCenter and external center aligned.
			*/
			mat = _image.transform.matrix.clone();
			
			/* 
			The matchInternalPointWithExternal method of the MatrixTransformer class changes the mat matrix to include a
			translation that makes the point in terms of image coordinates line up with the point in terms of spImage
			coordinates.
			*/
			MatrixTransformer.matchInternalPointWithExternal(mat, internalCenter, externalCenter);
			
			// Now applying the mat matrix to the enlarged image will align the internalCenter to the externalCenter.
			_image.transform.matrix = mat;
		}
		
		private static function onTweenUpdate():void
		{
			setMatrixTransform();
		}
		
		private static function onTweenComplete():void
		{
			setMatrixTransform();
		}
	}
}