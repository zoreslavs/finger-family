package com.utils
{
	public class VideoEnablingHelper
	{
		public static function get isVideoEnabled():Boolean
		{
			return true;//(PlatformCheckHelper.isIOS || (PlatformCheckHelper.isAndroid && AndroidDeviceHelper.isVideoEnabled));
		}
		
		public static function get isVideoRealtimeEnabled():Boolean
		{
			return (PlatformCheckHelper.isIOS || (PlatformCheckHelper.isAndroid && AndroidDeviceHelper.isVideoRealtimeEnabled));
		}
	}
}