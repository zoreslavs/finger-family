package com.utils
{
	public class TextCheckHelper
	{
		public static function isEmptyText(text:String):Boolean
		{
			var rex:RegExp = /[\s\r\n]+/gim;
			return (text.replace(rex,'') == "");
		}
		
		public static function isValidEmail(email:String):Boolean
		{
			var emailExpression:RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
			return emailExpression.test(email);
		}
	}
}