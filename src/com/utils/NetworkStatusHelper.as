package com.utils
{
	import com.Application;
	import com.abstract.INetworkStatusHelper;
	import com.freshplanet.nativeExtensions.AirNetworkInfo;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	
	public class NetworkStatusHelper implements INetworkStatusHelper
	{
		public function NetworkStatusHelper()
		{
			init();
		}
		
		private function init():void
		{
			NativeApplication.nativeApplication.addEventListener(Event.NETWORK_CHANGE, onNetworkChange);
		}
		
		private function onNetworkChange(e:Event):void
		{
			trace("onNetworkChange: ", isConnected);
			if (isConnected)
				Application.instance.store.init();
		}
		
		// get/set
		public function get isConnected():Boolean { return AirNetworkInfo.networkInfo.isConnected(); }
	}
}